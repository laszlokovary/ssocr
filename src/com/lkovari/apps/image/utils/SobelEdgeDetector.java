/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: SobelEdgeDetector.java
 * Created: 2013.11.03. 0:41:01
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class SobelEdgeDetector {
    private BufferedImage image = null;
    private byte[] pixelsGRAY;
    private boolean scaleIntensity = true;
    
    public SobelEdgeDetector() {
    }

    public int getPixel(int y, int x){
        int value = -1;
        try {
            value = image.getRGB(x, y);;
        }
        catch (Exception e) {
            System.out.println("X " + x + " Y " + y);
        }
        return value;
    }
    
    public void setPixel(BufferedImage buffImgOut, int x, int y, int value){
        buffImgOut.setRGB(x, y, value);
//        DataBufferByte dbb = (DataBufferByte) buffImgOut.getRaster().getDataBuffer();
//        dbb.setElem(x, y, value);
    }
    
    
    public BufferedImage execute(BufferedImage buffImgIn) {
        this.image = buffImgIn;
        int height = image.getHeight();
        int width = image.getWidth();
        System.out.println("W " + width + " H " + height);
        BufferedImage buffImgOut = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);

        double g, max = 0;
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                
                int p1 = getPixel(y - 1, x - 1);
                int p2 = getPixel(y - 1, x);
                int p3 = getPixel(y - 1, x + 1);
                int p4 = getPixel(y, x + 1);
                int p5 = getPixel(y + 1, x);
                int p6 = getPixel(y + 1, x + 1);
                int p7 = getPixel(y + 1, x - 1);
                int p8 = getPixel(y, x - 1);

                g = Math.min(255, Math.abs(p1 + p3 - p7 - p2 + 2 * (p2 - p5)) + Math.abs(p2 + p6 - p1 - p7 + 2 * (p4 - p8)));
                if (g > max) 
                    max = g;
                
                setPixel(buffImgOut, x, y, (int)g);
            }
        }
        
        if (scaleIntensity && max != 255){
            double factor = 255.0 / (double) max;
            
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    setPixel(buffImgOut, x, y, (int)(getPixel(x, y) * factor));
                }
            }
        }
        return buffImgOut;
    }
}
   