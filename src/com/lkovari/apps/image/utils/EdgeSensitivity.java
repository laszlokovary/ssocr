/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: EdgeSensitivity.java
 * Created: 2013.11.02. 23:55:28
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

public enum EdgeSensitivity {
    HIGH_EDGE_SENSITIVITY,
    MEDIUM_EDGE_SENSITIVITY,
    LOW_EDGE_SENSITIVITY;
}
