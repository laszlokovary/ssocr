/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: Opening.java
 * Created: 2013.11.02. 23:05:11
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;
import java.awt.image.BufferedImage;

/**
 * Opening operation for grayscaled images. The opening operand is the
 * {@link Dilation} of the {@link Erosion}.
 *
 * The opening operation will remove small "objects", i.e. brighter parts, in the image.
 * This operation can for instance be useful to remove uninteresting structures
 * yielding better accuracy when performing edge detection.
 *
 *
 */
public class Opening extends MorphOperations {
    private int shapeSize;
    private short[][] structElem;
    private StructElementShape seShape;

    public Opening() {
        this.shapeSize = 2;
        this.seShape = StructElementShape.SQUARE;
        this.structElem = constructShape(this.seShape, this.shapeSize);
    }

    public Opening(StructElementShape shape, int shapeSize) {
        this.shapeSize = shapeSize;
        this.seShape = shape;
        this.structElem = constructShape(this.seShape, this.shapeSize);
    }

    public BufferedImage execute(BufferedImage img) {
        if (img.getType() != BufferedImage.TYPE_BYTE_GRAY)
            throw new IllegalArgumentException("The image must be of type TYPE_BYTE_GRAY");

        BufferedImage erodedImg, openedImg;
        Dilation dilation = new Dilation(seShape, shapeSize);
        Erosion erosion = new Erosion(seShape, shapeSize);

        erodedImg = erosion.execute(img);
        openedImg = dilation.execute(erodedImg);
        dilation = null;
        erosion = null;
        return openedImg;
    }

}
