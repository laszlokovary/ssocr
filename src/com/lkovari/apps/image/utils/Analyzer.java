/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: Analyzer.java
 * Created: 2013.11.02. 15:16:05
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */

package com.lkovari.apps.image.utils;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

class Analyzer{
    private int pixdata[][];
    private int rgbdata[][];
    private BufferedImage image;
    int background_color;
    int border_color;
    int imagebg_color;
    private void populateRGB(){
        rgbdata = new int[image.getWidth()][image.getHeight()];
        for(int i = 0; i < image.getWidth(); i++){
            for(int j = 0; j < image.getHeight(); j++){
                rgbdata[i][j] = image.getRGB(i, j);
            }
        }
        int howmanydone = 0;
        int prevcolor,newcolor;
        prevcolor = rgbdata[0][0];

        /*
        for(int i = 0; i < image.getWidth(); i++){
           for(int j = 0; j < image.getHeight(); j++){
              System.out.print(rgbdata[i][j]);
           }
           System.out.println("");
        }*/
        for(int i = 0; i < image.getWidth(); i++){
            for(int j = 0; j < image.getHeight(); j++){
                newcolor = rgbdata[i][j];
                if((howmanydone == 0) && (newcolor != prevcolor)){
                    background_color = prevcolor;
                    border_color = newcolor;
                    prevcolor = newcolor;
                    howmanydone = 1;
                }
                if((newcolor != prevcolor) && (howmanydone == 1)){
                    imagebg_color = newcolor;
                }
            }
        }
    }
    public Analyzer() { 
        background_color = 0; 
        border_color = 0; 
        imagebg_color = 0;
   }
    public int background() { 
        return background_color; 
    }
    public int border() { 
        return border_color;
    }
    public int imagebg() {
        return imagebg_color;
    }
    public int analyze(String filename,String what) throws IOException{
        image = ImageIO.read(new File(filename + ".jpg"));
        pixdata = new int[image.getHeight()][image.getWidth()];
        populateRGB();
        if(what.equals("background"))
            return background();
        if(what.equals("border"))
            return border();
        if(what.equals("image-background"))
            return imagebg();
        else return 0;
    }
    
    
    public static void main(String[] args){
        Analyzer an = new Analyzer();
        String imageName = "F://img//img_2346";
        
//        Scanner scan = new Scanner(System.in);
//        System.out.print("Enter image name:");
//        imageName = scan.nextLine();
        try{
          //"border","image-background","background" will get you different colors
          int a = an.analyze(imageName,"border");
          Color c = new Color(a);
          System.out.printf("Color bg: %x", a);
          System.out.println(" R " + c.getRed() + " G " + c.getGreen() + " B " + c.getBlue());
          a = an.analyze(imageName,"background");
          c = new Color(a);
          System.out.printf("Color bg: %x",a);
          System.out.println(" R " + c.getRed() + " G " + c.getGreen() + " B " + c.getBlue());
          a = an.analyze(imageName,"image-background");
          c = new Color(a);
          System.out.printf("Color bg: %x",a);
          System.out.println(" R " + c.getRed() + " G " + c.getGreen() + " B " + c.getBlue());
          

        }catch(Exception e){
           System.out.println(e.getMessage());
        }
    }
    
}
