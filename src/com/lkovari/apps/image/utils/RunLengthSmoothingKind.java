/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: RunLengthSmoothingKind.java
 * Created: 2013.12.25. 10:39:52
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;
/**
 * 
 * @author lkovari
 * RunLengthSmoothingKind
 */
public enum RunLengthSmoothingKind {
    HORIZONTAL,
    VERTICAL,
    BOTH_OR,
    BOTH_AND;
}
