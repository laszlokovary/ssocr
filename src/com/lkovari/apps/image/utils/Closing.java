/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: Closing.java
 * Created: 2013.11.02. 22:54:17
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;
import java.awt.image.BufferedImage;

/**
 * Closing operation for grayscaled images. The closing operand is the
 * {@link Erosion} of the {@link Dilation}.
 *
 * The closing operation will remove "holes", i.e. darker parts, in the image.
 * This operation can for instance be useful to remove uninteresting structures
 * yielding better accuracy when performing edge detection.
 *
 *
 */
public class Closing extends MorphOperations {
    private int shapeSize;
    private short[][] structElem;
    private StructElementShape seShape;

    public Closing() {
        this.shapeSize = 3;
        this.seShape = StructElementShape.SQUARE;
        this.structElem = constructShape(this.seShape, this.shapeSize);
    }

    public Closing(StructElementShape shape, int shapeSize) {
        this.shapeSize = shapeSize;
        this.seShape = shape;
        this.structElem = constructShape(this.seShape, this.shapeSize);
    }

    public BufferedImage execute(BufferedImage img) {
        if (img.getType() != BufferedImage.TYPE_BYTE_GRAY)
            throw new IllegalArgumentException("The image must be of type TYPE_BYTE_GRAY");

        BufferedImage dilatedImg, closedImg;
        // growing the objects
        Dilation dilation = new Dilation(this.seShape, this.shapeSize);
        // press the objects
        Erosion erosion = new Erosion(this.seShape, this.shapeSize);

        dilatedImg = dilation.execute(img);
        closedImg = erosion.execute(dilatedImg);
        erosion = null;
        dilation = null;
        return closedImg;
    }

}
