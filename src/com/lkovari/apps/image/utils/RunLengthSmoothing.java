/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: RunLengthSmoothing.java
 * Created: 2013.12.03. 21:48:33
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class RunLengthSmoothing {
    private static int threshold = 4;
    

    /**
     * 
     * Method: copyImageColors 
     * @param buffImgIn
     * @param matrix
     * @return
     */
    private static int[][] copyImageColors(BufferedImage buffImgIn, int[][] matrix) {
        for (int x = 0; x < buffImgIn.getWidth(); x++) {
            for (int y = 0; y < buffImgIn.getHeight(); y++) {
                matrix[x][y] = buffImgIn.getRGB(x, y);
            }
        }    
        return matrix;
    }
    
    public static BufferedImage execute(BufferedImage buffImgIn, int th, RunLengthSmoothingKind rlsKind) {
        threshold = th;
        BufferedImage buffImgOut = new BufferedImage(buffImgIn.getWidth(), buffImgIn.getHeight(), buffImgIn.getType());
        
        Color commonColor = ImageUtils.detectMostCommonColor(buffImgIn);
        Color pixelColor = Color.BLACK;
        
        if (commonColor.equals(Color.WHITE))
            pixelColor = Color.BLACK;
        else
            pixelColor = Color.WHITE;

        // copy colors to matrix
        int[][] horizontalMatrix = new int[buffImgIn.getWidth()][buffImgIn.getHeight()];
        horizontalMatrix = copyImageColors(buffImgIn, horizontalMatrix);

        // processing horizontally at first
        for (int y = 0; y < buffImgIn.getHeight(); y++) {
            int fromXIx = 0;
            int cntBgPx = 0;
            while (fromXIx < buffImgIn.getWidth()) {
                int colorRgb = buffImgIn.getRGB(fromXIx, y);
                // is equals with background
                if (colorRgb == commonColor.getRGB()) {
                    cntBgPx++;    
                }
                else if (colorRgb != commonColor.getRGB()) {
                    if (cntBgPx > 0) {
                        if (cntBgPx < threshold) {
                            int startIx = fromXIx - cntBgPx;
                            // set to black to
                            for (int fIx = startIx; fIx <= fromXIx; fIx++) {
                                horizontalMatrix[fIx][y] = pixelColor.getRGB();
                            }
                        }
                        cntBgPx = 0;
                    }
                }
                fromXIx++;
            }
        }
        
        int[][] verticalMatrix = new int[buffImgIn.getWidth()][buffImgIn.getHeight()];
        // copy colors to matrix
        verticalMatrix = copyImageColors(buffImgIn, verticalMatrix);
        
        // processing vertically at second
        for (int x = 0; x < buffImgIn.getWidth(); x++) {
            int fromYIx = 0;
            int cntBgPx = 0;
            while (fromYIx < buffImgIn.getHeight()) {
                int colorRgb = buffImgIn.getRGB(x, fromYIx);
                // is equals with background
                if (colorRgb == commonColor.getRGB()) {
                    cntBgPx++;    
                }
                else if (colorRgb != commonColor.getRGB()) {
                    if (cntBgPx > 0) {
                        if (cntBgPx < threshold) {
                            int startIx = fromYIx - cntBgPx;
                            // set to black to
                            for (int fIx = startIx; fIx <= fromYIx; fIx++) {
                                verticalMatrix[x][fIx] = pixelColor.getRGB();
                            }
                        }
                        cntBgPx = 0;
                    }
                }
                fromYIx++;
            }
        }
        // set 
        for (int x = 0; x < buffImgIn.getWidth(); x++) {
            for (int y = 0; y < buffImgIn.getHeight(); y++) {
                int value = buffImgIn.getRGB(x, y);
                
                switch (rlsKind) {
                case BOTH_AND : {
                    value = horizontalMatrix[x][y] & verticalMatrix[x][y];
                    break;
                }
                case BOTH_OR : {
                    value = horizontalMatrix[x][y] | verticalMatrix[x][y];
                    break;
                }
                case HORIZONTAL : {
                    value = horizontalMatrix[x][y];
                    break;
                }
                case VERTICAL : {
                    value = verticalMatrix[x][y];
                    break;
                }
                default:
                    break;
                }
                
                buffImgOut.setRGB(x, y, value);
            }
        }
        return buffImgOut;
    }
    
}
