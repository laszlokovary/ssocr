/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: MorphOperations.java
 * Created: 2013.11.02. 21:48:52
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

public class MorphOperations {

    
    protected short[][] constructShape(StructElementShape seShape, int shapeSize) {
        int size = 2 * shapeSize + 1;
        short[][] structElem = new short[size][size];
        switch (seShape) {
        case SQUARE: {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                        structElem[i][j] = 1;
                }
            }
            break;
        }        
        case VERTICAL_LINE: {
            for (int i = 0; i < size; i++) {
                structElem[i][shapeSize] = 1;
            }
            break;
        }        
        case HORIZONTAL_LINE: {
            for (int i = 0; i < size; i++) {
                structElem[shapeSize][i] = 1;
            }
            break;
        }        
        default:
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    structElem[i][j] = 1;
                }
            }
        }
        return structElem;
    }    
}
