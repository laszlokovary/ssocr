/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: StructElementShape.java
 * Created: 2013.10.27. 8:40:37
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

public enum StructElementShape {
    SQUARE,
    VERTICAL_LINE,
    HORIZONTAL_LINE,
    CIRCLE;
}
