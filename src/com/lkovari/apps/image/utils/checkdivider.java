/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: checkdivider.java
 * Created: 2013.11.17. 10:58:58
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

public class checkdivider {

    /**
     * Method: main 
     * @param args
     */
    public static void main(String[] args) {
        int[] res = new int[20];
        for (int i=2; i<=1000; i++) {
            for (int d=1; d<20; d++) {
                if ((i % d) == 0) {
                    res[d]++;
                }
            }
        }
        System.out.println("RES ");
        for (int i=0; i<20;i++) {
            System.out.println(" " + i + " = " + res[i]);
        }
    }

}
