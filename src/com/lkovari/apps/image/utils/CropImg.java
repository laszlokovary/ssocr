/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: CropImg.java
 * Created: 2013.10.25. 21:53:12
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

/**
 * 
 * @author lkovari CropImg
 * 
 *         Determinate most common color :  http://stackoverflow.com/questions/4427200/getting-the-most-common-color-of-a-image 
 *         Determinate difference of colors : http://stackoverflow.com/questions/2103368/color-logic-algorithm
 *         Finding similar colors : http://stackoverflow.com/questions/1725505/finding-similar-colors-programatically 
 *         Autocrop white border : http://stackoverflow.com/questions/10678015/how-to-auto-crop-an-image-white-border-in-java 
 *         Transparent color : http://stackoverflow.com/questions/665406/how-to-make-a-color-transparent-in-a-bufferedimage-and-save-as-png 
 *         Edge detection : http://processing.org/examples/edgedetection.html 
 *         Edge detection : http://stackoverflow.com/questions/5827809/canny-edge-detection-using-processing 
 *         Edge JAI example : https://forums.oracle.com/thread/1267604
 *         
 *         Document sizes example : 
 *         Page: 2480x3508  Card: 1010x644 Ratio 8699840 : 650440 1% = 86998.4 Card = 7.47% of Paper
 *         Page: 1275x1650  Card: 326x501 Ratio 2103750 : 163326 1% = 21037,5 Card = 7.77% of Paper
 *         Page: 4960x7016  Card: 2191x1573 Ratio 34799360 : 3446443 1% = 347993,6 Card = 9.9% of Paper 
 *         Page: 1240x1754  Card: 506x323 Ratio 2174960 : 163438 1% = 21749,6 Card = 7.5% of Paper
 *         Page: 620x877  Card: 274x198 Ratio 543740 : 54252 1% = 5437,4 Card = 9.9% of Paper
 */
public class CropImg {
    private BufferedImage img;
    private BufferedImage imgbw = null;
    private BufferedImage imgGray = null;
    private BufferedImage imgOtsuBin = null;
    private BufferedImage imgReduced = null;
    private BufferedImage imgRLS = null;

    private Color binCommonColor = null;
    private Color grayCommonColor = null;
    private Color bwCommonColor = null;

    private Map<Color, Integer> colors = new HashMap<Color, Integer>();
    // private double tolerance = 2.3456745674;
    private double tolerance1 = 412.0; // true 412
    private double tolerance2 = 220.0; // false 712
    private boolean isCalculateDistanceWithOldMethod = true;
    private double tolerance = isCalculateDistanceWithOldMethod ? tolerance1 : tolerance2;
    private double DISTANCE_MAX1 = 764.8333151739665;
    private double DISTANCE_MAX2 = 441.6729559300637;
    private double DISTANCE_MAX = isCalculateDistanceWithOldMethod ? DISTANCE_MAX1 : DISTANCE_MAX2;

    private String fileName = null;
    
    private boolean isStoreTempFiles = false;
    
    private int CONST_GROOW = 50;
    
    private double MIN_PERCENT = 3.0;
    private double MAX_PERCENT = 25.0;

    private Rectangle projRect = new Rectangle();
    private Rectangle diffRect = new Rectangle();
    private Rectangle rect = new Rectangle();
    
    public CropImg() {
        super();
    }

    /**
     * 
     * Method: readFile
     * 
     * @param fn
     */
    public void readFile(String fn) {
        try {
            this.fileName = fn;
            File input = new File(fn + ".jpg");
            img = ImageIO.read(input);
        } catch (IOException e) {
            throw new RuntimeException("Can't read imag e" + fn, e);
        }
    }

    /**
     * 
     * Method: detectMostCommonColor
     * 
     * @param img
     * @return
     */
    private Color detectMostCommonColor(BufferedImage buffImgIn) {
        int width = buffImgIn.getWidth();
        int height = buffImgIn.getHeight();
        for (int y = height - 1; y >= 0; y--) {
            for (int x = width - 1; x >= 0; x--) {
                Color c = new Color(buffImgIn.getRGB(x, y));
                boolean isFound = colors.containsKey(c);
                if (!isFound) {
                    colors.put(c, new Integer(1));
                } else {
                    colors.put(c, colors.get(c) + 1);
                }
            }
        }
        List list = new LinkedList(colors.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });
        Color mostCommonColor = null;
        Map.Entry mcc = (Map.Entry) list.get(list.size() - 1);
        mostCommonColor = (Color) mcc.getKey();
        list.clear();
        list = null;
        colors.clear();
        return mostCommonColor;
    }

    /**
     * 
     * Method: createBufferedImageFrom
     * 
     * @param srcBuffImg
     * @return
     */
    private BufferedImage createBufferedImageFrom(BufferedImage srcBuffImg,
            boolean isGray) {
        int width = srcBuffImg.getWidth();
        int height = srcBuffImg.getHeight();
        BufferedImage bufferedImage = null;
        if (isGray) {
            bufferedImage = new BufferedImage(width, height,
                    BufferedImage.TYPE_BYTE_GRAY);
        } else {
            bufferedImage = new BufferedImage(width, height,
                    BufferedImage.TYPE_BYTE_BINARY);
        }
        final Graphics2D g2d = (Graphics2D) bufferedImage.getGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR); // or .._BICUBIC
        g2d.drawImage(srcBuffImg, 0, 0, null);
        g2d.dispose();
        return bufferedImage;
    }

    /**
     * 
     * Method: convertToBlackAndWhite
     * 
     * @return
     */
    private BufferedImage convertToBlackAndWhite(BufferedImage imgIn) {
        BufferedImage imgw = null;
        if (imgIn == null) {
            imgw = this.img;
        } else {
            imgw = imgIn;
        }
        BufferedImage bufferedImage = createBufferedImageFrom(imgw, false);
        return bufferedImage;
    }

    private BufferedImage convertToErodeImage(BufferedImage buffImgIn) {
        BufferedImage img = null;
        if (buffImgIn == null) {
            img = this.img;
        } else {
            img = buffImgIn;
        }
        Erosion erosion = new Erosion();
        BufferedImage erodeImage = erosion.execute(img);
        erosion = null;
        return erodeImage;
    }

    private BufferedImage convertToDilateImage(BufferedImage buffImgIn) {
        BufferedImage img = null;
        if (buffImgIn == null) {
            img = this.img;
        } else {
            img = buffImgIn;
        }
        Dilation dilation = new Dilation();
        BufferedImage dilateImage = dilation.execute(img);
        dilation = null;
        return dilateImage;
    }

    public BufferedImage closingImage(BufferedImage buffImgIn,
            StructElementShape shape, int size) {
        BufferedImage img = null;
        if (buffImgIn == null) {
            img = this.img;
        } else {
            img = buffImgIn;
        }
        Closing closing = new Closing(shape, size);
        BufferedImage closingImage = closing.execute(img);
        closing = null;
        return closingImage;
    }

    public BufferedImage openingImage(BufferedImage buffImgIn,
            StructElementShape shape, int size) {
        BufferedImage img = null;
        if (buffImgIn == null) {
            img = this.img;
        } else {
            img = buffImgIn;
        }
        Opening opening = new Opening(shape, size);
        BufferedImage openingImage = opening.execute(img);
        opening = null;
        return openingImage;
    }

    /**
     * 
     * Method: convertToGrayScale
     * 
     * @return
     */
    private BufferedImage convertToGrayScale(BufferedImage imgIn) {
        BufferedImage img = null;
        if (imgIn == null) {
            img = this.img;
        } else {
            img = imgIn;
        }
        BufferedImage bufferedImage = createBufferedImageFrom(img, true);
        ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        op.filter(img, bufferedImage);
        op = null;
        return bufferedImage;
    }

    private BufferedImage convertImageToBufferedImage(Image image, int width,
            int height) {
        BufferedImage dest = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = dest.createGraphics();
        g2.drawImage(image, 0, 0, null);
        g2.dispose();
        return dest;
    }

    private BufferedImage transformGrayToTransparency(BufferedImage image) {
        ImageFilter filter = new RGBImageFilter() {
            public final int filterRGB(int x, int y, int rgb) {
                return (rgb << 8) & 0xFF000000;
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        Image imageOut = Toolkit.getDefaultToolkit().createImage(ip);
        return convertImageToBufferedImage(imageOut, imageOut.getWidth(null),
                imageOut.getHeight(null));
    }

    /**
     * 
     * Method: calculateAverageOfArea
     * 
     * @param x
     * @param y
     * @param w
     * @param h
     * @return
     */
    private double calculateAverageOfArea(int x, int y, int w, int h) {
        double avgColor = 0.0;
        long cnt = 0;
        /*
         * double sumColor = 0.0; for(int i = x; i < x + w; i++) { for(int j = y
         * ; j <= y + h; j++) { long c = img.getRGB(x, y); sumColor += c; cnt++;
         * } } avgColor = sumColor / cnt;
         */
        int a = 0;
        int r = 0;
        int g = 0;
        int b = 0;
        for (int i = x; i < x + w; i++) {
            for (int j = y; j <= y + h; j++) {
                int c = this.img.getRGB(x, y);
                Color cc = new Color(c);
                a += cc.getAlpha();
                r += cc.getRed();
                g += cc.getGreen();
                b += cc.getBlue();
                cnt++;
            }
        }
        float avgA = a / cnt;
        float avgR = r / cnt;
        float avgG = g / cnt;
        float avgB = b / cnt;
        Color ccc = new Color(avgR, avgG, avgB, avgA);
        avgColor = ccc.getRGB();
        return avgColor;
    }

    /**
     * 
     * Method: write
     * 
     * @param f
     */
    public void write(File f, String type) {
        try {
            ImageIO.write(this.img, type, f);
        } catch (IOException e) {
            throw new RuntimeException("Can't write image", e);
        }
    }

    /**
     * 
     * Method: writebw
     * 
     * @param f
     */
    public void writebw(File f, BufferedImage img, String type) {
        try {
            ImageIO.write(img, type, f);
        } catch (IOException e) {
            throw new RuntimeException("Can't write image", e);
        }
    }

    /**
     * 
     * Method: calculateColorDistance2
     * 
     * @param refColorRgb
     * @param imgColorRgb
     * @return
     */
    private double calculateColorDistance2(int refColorRgb, int imgColorRgb) {

        int rAlpha = (int) ((refColorRgb & 0xFF000000) >>> 24); // Alpha level
        int rRed = (int) ((refColorRgb & 0x00FF0000) >>> 16); // Red level
        int rGreen = (int) ((refColorRgb & 0x0000FF00) >>> 8); // Green level
        int rBlue = (int) (refColorRgb & 0x000000FF); // Blue level

        int iAlpha = (int) ((imgColorRgb & 0xFF000000) >>> 24); // Alpha level
        int iRed = (int) ((imgColorRgb & 0x00FF0000) >>> 16); // Red level
        int iGreen = (int) ((imgColorRgb & 0x0000FF00) >>> 8); // Green level
        int iBlue = (int) (imgColorRgb & 0x000000FF); // Blue level

        long rmean = ((long) rRed + (long) iRed) / 2;
        long r = (long) rRed - (long) iRed;
        long g = (long) rGreen - (long) iGreen;
        long b = (long) rBlue - (long) iBlue;
        return Math.sqrt((((512 + rmean) * r * r) >> 8) + 4 * g * g
                + (((767 - rmean) * b * b) >> 8));
    }

    /**
     * 
     * Method: calculateColorDistance1
     * 
     * @param refColorRgb
     * @param imgColorRgb
     * @return
     */
    private double calculateColorDistance1(int refColorRgb, int imgColorRgb) {
        double distance = 0.0;
        int aAlpha = (int) ((refColorRgb & 0xFF000000) >>> 24); // Alpha level
        int aRed = (int) ((refColorRgb & 0x00FF0000) >>> 16); // Red level
        int aGreen = (int) ((refColorRgb & 0x0000FF00) >>> 8); // Green level
        int aBlue = (int) (refColorRgb & 0x000000FF); // Blue level

        int bAlpha = (int) ((imgColorRgb & 0xFF000000) >>> 24); // Alpha level
        int bRed = (int) ((imgColorRgb & 0x00FF0000) >>> 16); // Red level
        int bGreen = (int) ((imgColorRgb & 0x0000FF00) >>> 8); // Green level
        int bBlue = (int) (imgColorRgb & 0x000000FF); // Blue level

        distance = Math.sqrt((aAlpha - bAlpha) * (aAlpha - bAlpha)
                + (aRed - bRed) * (aRed - bRed) + (aGreen - bGreen)
                * (aGreen - bGreen) + (aBlue - bBlue) * (aBlue - bBlue));
        return distance;
    }

    /**
     * 
     * Method: isColorEquals
     * 
     * @param refColorRGB
     * @param colorRGB
     * @return
     */
    private boolean isColorEquals(int refColorRGB, int colorRGB) {
        return refColorRGB == colorRGB;
    }

    /**
     * 
     * Method: calculateAreaPoints
     * 
     * @param commonColor
     * @param isBlackAndWhite
     * @return
     */
    private List<Point> calculateAreaPoints(Color commonColor,
            BufferedImage buffImg) {
        List<Point> areaPoints = new ArrayList<Point>();
        int height = -1;
        int width = -1;
        if (buffImg == null) {
            height = this.imgbw.getHeight();
            width = this.imgbw.getWidth();
        } else {
            height = buffImg.getHeight();
            width = buffImg.getWidth();
        }
        for (int y = height - 1; y >= 0; y--) {
            for (int x = width - 1; x >= 0; x--) {
                double diff = 0.0;
                if (buffImg != null) {
                    // with Gray scale
                    if (this.isCalculateDistanceWithOldMethod)
                        diff = calculateColorDistance1(commonColor.getRGB(),
                                buffImg.getRGB(x, y));
                    else
                        diff = calculateColorDistance2(commonColor.getRGB(),
                                buffImg.getRGB(x, y));
                    if (diff > 0) {
                        if (diff > tolerance) {
                            // System.out.println("X " + x + " Y " + y);
                            areaPoints.add(new Point(x, y));
                        }
                    }
                } else {
                    if (!isColorEquals(commonColor.getRGB(),
                            this.imgbw.getRGB(x, y))) {
                        areaPoints.add(new Point(x, y));
                    }
                }
            }
        }
        return areaPoints;
    }

    /**
     * 
     * Method: calculateCroppingArea
     * 
     * @param trimColor
     * @return
     */
    private Rectangle calculateCroppingArea(BufferedImage buffImg) {
        Rectangle rect = new Rectangle();
        /*
         * AffineTransform transform = AffineTransform.getScaleInstance(0.5,
         * 0.5); AffineTransformOp op = new AffineTransformOp(transform,
         * AffineTransformOp.TYPE_NEAREST_NEIGHBOR); imgGray =
         * op.filter(this.img, null);
         * 
         * double avgColor = calculateAverageOfArea(100,26,660,125); int
         * lAvgColor = (int) avgColor; double dff =
         * calculateColorDistance1(Color.WHITE.getRGB(), lAvgColor); dff =
         * calculateColorDistance2(Color.WHITE.getRGB(), lAvgColor);
         */
        // imgGray = transformGrayToTransparency(imgGray);
        List<Point> areaPoints = calculateAreaPoints(this.grayCommonColor, buffImg);
        // maybe too small?
        if (areaPoints.size() < 3000) {
            // try with black and white method
            areaPoints = calculateAreaPoints(this.bwCommonColor, this.imgbw);
            if (areaPoints.size() < 3000) {
                rect.width = 10;
                rect.height = 10;
                rect.x = 0;
                rect.y = 0;
            }
        }
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        int maxY = Integer.MIN_VALUE;
        // System.out.println("PTs " + areaPoints.size());
        for (Point pt : areaPoints) {
            if (minX > pt.x)
                minX = pt.x;
            if (minY > pt.y)
                minY = pt.y;
            if (maxX < pt.x)
                maxX = pt.x;
            if (maxY < pt.y)
                maxY = pt.y;
        }
        areaPoints.clear();
        areaPoints = null;
        rect.width = (maxX - minX);
        rect.height = (maxY - minY);
        rect.x = minX;
        rect.y = minY;
        return rect;
    }

    /**
     * 
     * Method: invertImage
     * 
     * @param buffImgIn
     * @return
     */
    private BufferedImage invertImage(BufferedImage buffImgIn) {
        int height = this.imgbw.getHeight();
        int width = this.imgbw.getWidth();
        for (int x = 1; x < width - 1; x++) {
            for (int y = 1; y < height - 1; y++) {
                int rgba = buffImgIn.getRGB(x, y);
                Color col = new Color(rgba, true);
                col = new Color(255 - col.getRed(), 255 - col.getGreen(),
                        255 - col.getBlue());
                buffImgIn.setRGB(x, y, col.getRGB());
            }
        }
        return buffImgIn;
    }

    /**
     * 
     * Method: erosion
     * 
     * @param buffImg
     * @return
     */
    private BufferedImage erosion(BufferedImage buffImg) {
        int height = this.imgbw.getHeight();
        int width = this.imgbw.getWidth();
        BufferedImage buffImgOut = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_ARGB);
        final int BLACK = 0xFF000000;
        final int WHITE = 0xFFFFFFFF;
        for (int x = 1; x < width - 1; x++) {
            for (int y = 1; y < height - 1; y++) {
                // System.out.println(Integer.toHexString(src.getRGB(x, y)));
                // a cross-like kernel (left, right, top, bottom)
                if (buffImg.getRGB(x - 1, y) != WHITE //
                        || buffImg.getRGB(x + 1, y) != WHITE //
                        || buffImg.getRGB(x, y - 1) != WHITE //
                        || buffImg.getRGB(x, y + 1) != WHITE//
                ) {
                    // if one neighbor pixel is black, make this one black too
                    buffImgOut.setRGB(x, y, BLACK);
                } else {
                    buffImgOut.setRGB(x, y, WHITE);
                }
            }
        }
        return buffImgOut;
    }

    /*
     * private BufferedImage convertToEdgedImage(BufferedImage buffImg) {
     * EdgeDetector edgeDetector = new EdgeDetector(); BufferedImage edgeImage =
     * edgeDetector.convertToEdgeImgae(buffImg); edgeDetector = null; return
     * edgeImage; }
     */

    private BufferedImage convertToEdgeImageCanny(BufferedImage buffImgIn,
            EdgeSensitivity edgeSensitivity) {
        int width = 0;
        int height = 0;
        BufferedImage imgw = null;
        if (buffImgIn == null) {
            width = this.img.getWidth();
            height = this.img.getHeight();
            imgw = this.img;
        } else {
            width = buffImgIn.getWidth();
            height = buffImgIn.getHeight();
            imgw = buffImgIn;
        }

        CannyEdgeDetector detector = new CannyEdgeDetector();
        // 0.5f
        detector.setEdgeSensitivity(edgeSensitivity);
        detector.setSourceImage(buffImgIn);
        detector.execute();
        BufferedImage edgeImage = detector.getEdgesImage();
        detector = null;
        /*
         * EdgeDetectorCanny detector = new EdgeDetectorCanny(); BufferedImage
         * edgeImage = detector.execute(imgw);
         */
        return edgeImage;
    }

    private BufferedImage convertToEdgeImageSobel(BufferedImage buffImgIn) {
        SobelEdgeDetector detector = new SobelEdgeDetector();
        BufferedImage edges = detector.execute(buffImgIn);
        detector = null;
        return edges;
    }

    private BufferedImage processMedianFilter(BufferedImage buffImgIn, int size) {
        BufferedImage buffImgOut = new BufferedImage(buffImgIn.getWidth(), buffImgIn.getHeight(), BufferedImage.TYPE_INT_ARGB);
        MedianFilter mf = new MedianFilter(size);
        mf.filter(buffImgIn, buffImgOut);
        mf = null;
        return buffImgOut;
    }

    private double calculateMaxColorDifferenceOfImage(BufferedImage buffImg) {
        int width = buffImg.getWidth();
        int height = buffImg.getHeight();
        int minColor = Integer.MAX_VALUE;
        int maxColor = Integer.MIN_VALUE;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int colorRGB = buffImg.getRGB(x, y);
                if (minColor > colorRGB)
                    minColor = colorRGB;
                if (maxColor < colorRGB)
                    maxColor = colorRGB;
            }
        }
        double diff = calculateColorDistance1(minColor, maxColor);
        return diff;
    }
    


    /**
     * 
     * Method: calculateBoundingBoxWithEdgeDetection 
     * @param buffImg
     * @return
     */
    private Rectangle calculateBoundingBoxWithEdgeDetection(BufferedImage buffImg) {
        // convert to gray scale
        BufferedImage imgGray = convertToGrayScale(buffImg);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_g.png"), imgGray, "png");
        }    
        
        // convert to edge image
        BufferedImage imageEdge = convertToEdgeImageCanny(imgGray, EdgeSensitivity.MEDIUM_EDGE_SENSITIVITY);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_e.png"), imageEdge, "png");
        }    
        this.grayCommonColor = detectMostCommonColor(imageEdge);
        // convert to b&w
        this.imgbw = convertToBlackAndWhite(imgGray);
        this.bwCommonColor = detectMostCommonColor(this.imgbw);

        double diff = calculateMaxColorDifferenceOfImage(imageEdge);
        if (diff < DISTANCE_MAX) {
            this.tolerance = diff / 2;
        } else {
            if (this.isCalculateDistanceWithOldMethod)
                this.tolerance = this.tolerance1;
            else
                this.tolerance = this.tolerance2;
        }
        return calculateCroppingArea(imageEdge);
    }
    
    /**
     * 
     * Method: calculateBoundingBoxWithProjection 
     * @param buffImg
     * @return
     */
    private Rectangle calculateBoundingBoxWithProjection(BufferedImage buffImg) {
        Rectangle rect = new Rectangle();
        this.imgOtsuBin = OtsuBinarize.convertToBinary(buffImg, 0);
        this.binCommonColor = detectMostCommonColor(this.imgOtsuBin);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_ob.png"), this.imgOtsuBin, "png");
        }    
        
        int[] horizontalDifferences = Projection.calculateHorizontalProjectionFromBinImg(this.imgOtsuBin, this.binCommonColor.getRGB());
        int[] verticalDifferences = Projection.calculateVerticalProjectionFromBinImg(this.imgOtsuBin, this.binCommonColor.getRGB());
        Point projLTPoint = Projection.calculateLeftTopProjectionPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.PROJECTION_THRESHOLD);
        Point projRBPoint = Projection.calculateRightBottomDiffPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.PROJECTION_THRESHOLD);

        projLTPoint.x *= ImageUtils.RESIZE_RATIO;
        projLTPoint.y *= ImageUtils.RESIZE_RATIO;
        projRBPoint.x *= ImageUtils.RESIZE_RATIO;
        projRBPoint.y *= ImageUtils.RESIZE_RATIO;
        
        rect.x = projLTPoint.x;
        rect.y = projLTPoint.y;
        rect.width = projRBPoint.x - projLTPoint.x;
        rect.height = projRBPoint.y - projLTPoint.y;
        return rect;
    }

    /**
     * 
     * Method: calculateBoundingBoxWithDifferenceCount 
     * @param buffImg
     * @return
     */
    private Rectangle calculateBoundingBoxWithDifferenceCount(BufferedImage buffImgIn) {
        Rectangle rect = new Rectangle();
//        this.imgOtsuBin = OtsuBinarize.convertToBinary(buffImg);
        this.binCommonColor = detectMostCommonColor(buffImgIn);
        // tried the difference count methode 
        int[] horizontalDifferences = Projection.calculateHorizontalDifferenceSumFromBinImg(buffImgIn, this.binCommonColor.getRGB());
        int[] verticalDifferences = Projection.calculateVerticalDifferenceSumFromBinImg(buffImgIn, this.binCommonColor.getRGB());
        Point diffLTPoint = Projection.calculateLeftTopDiffPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.DIFFERENCE_THRESHOLD);
        Point diffRBPoint = Projection.calculateRightBottomDiffPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.DIFFERENCE_THRESHOLD);

        diffLTPoint.x *= ImageUtils.RESIZE_RATIO;
        diffLTPoint.y *= ImageUtils.RESIZE_RATIO;
        diffRBPoint.x *= ImageUtils.RESIZE_RATIO;
        diffRBPoint.y *= ImageUtils.RESIZE_RATIO;
        
        rect.x = diffLTPoint.x;
        rect.y = diffLTPoint.y;
        rect.width = diffRBPoint.x - diffLTPoint.x;
        rect.height = diffRBPoint.y - diffLTPoint.y;
        return rect;
    }
    
    
    /**
     * 
     * Method: cropSubimage 
     * @param img
     * @param rect
     * @return
     */
    private BufferedImage cropSubimage(BufferedImage img, Rectangle rect) {
        BufferedImage subImage = img.getSubimage(rect.x, rect.y, rect.width, rect.height);
        BufferedImage newImg = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = newImg.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.drawImage(subImage, 0, 0, null);
        this.img = newImg;
        g2d.dispose();
        return newImg;
    }
    
    /**
     * 
     * Method: isImageTooSmall 
     * @param currentRect
     * @param originWidth
     * @param originHeight
     * @return
     */
    private boolean isCropAreaTooSmall(Rectangle currentRect, int originWidth, int originHeight) {
        boolean isTooSmall = true;
        int originArea = originWidth * originHeight;
        int currentArea = currentRect.width * currentRect.height;
        double onePercent = originArea / 100;
        double croppedInPercent = currentArea / onePercent;
        double originWidthOnePercent = originWidth / 100;
        double originHeightOnePercent = originHeight / 100;
        double widthPercent = currentRect.width / originWidthOnePercent;
        double heightPercent = currentRect.height / originHeightOnePercent;
        isTooSmall = (croppedInPercent < this.MIN_PERCENT) && (widthPercent < 40) && (heightPercent < 20); 
        return isTooSmall;
    }

    /**
     * 
     * Method: isCropAreaTooLarge 
     * @param currentRect
     * @param originWidth
     * @param originHeight
     * @return
     */
    private boolean isCropAreaTooLarge(Rectangle currentRect, int originWidth, int originHeight) {
        boolean isTooSmall = true;
        int originArea = originWidth * originHeight;
        int currentArea = currentRect.width * currentRect.height;
        double onePercent = originArea / 100;
        double croppedInPercent = currentArea / onePercent;
        double originWidthOnePercent = originWidth / 100;
        double originHeightOnePercent = originHeight / 100;
        double widthPercent = currentRect.width / originWidthOnePercent;
        double heightPercent = currentRect.height / originHeightOnePercent;
        isTooSmall = (croppedInPercent < this.MAX_PERCENT) && (widthPercent > 85) && (heightPercent < 85); 
        return isTooSmall;
    }
    
    /**
     * 
     * Method: adjustRectangle 
     * @param rect
     * @param originWidth
     * @param originHeight
     * @param delta
     * @return
     */
    private Rectangle adjustRectangle(Rectangle rect, int originWidth, int originHeight, int delta) {
        
        if ((rect.width + delta) <= originWidth) {
            rect.width += delta; 
        }
        if ((rect.height + delta) <= originHeight) {
            rect.height += delta; 
        }
        if ((rect.x - delta) > 0) {
            rect.x -= delta; 
        }
        if ((rect.y - delta) > 0) {
            rect.y -= delta; 
        }
        return rect;
    }
    
    /**
     * 
     * Method: crop
     * 
     * @return
     */
    public boolean crop() {
        this.imgbw = null;
        this.imgGray = null;
        this.imgOtsuBin = null;
        this.imgReduced = null;
        this.imgRLS = null;
        
        boolean isSuccess = false;
        int originWidth = img.getWidth();
        int originHeight = img.getHeight();
        int originArea = originWidth * originHeight;
        // remove noise
        this.imgReduced = ImageUtils.resizeImage(this.img, originWidth / ImageUtils.RESIZE_RATIO, originHeight / ImageUtils.RESIZE_RATIO, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_r.png"), this.imgReduced, "png");
        }

        
        /*
        this.imgGray = ImageUtils.convertToGrayWithFilter(this.imgReduced);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_g.png"), this.imgGray, "png");
        }
        */
        
        /*
        BufferedImage buffImgBL = null; 
        BufferedImage buffImgBL = ImageUtils.removeBorders(this.imgReduced);
        this.imgOtsuBin = buffImgBL; 
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_bl.png"), buffImgBL, "png");
        } 
        */   

        this.imgOtsuBin = ImageUtils.convertToBinaryWithOtsu(this.imgReduced, 0);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_ob.png"), this.imgOtsuBin, "png");
        }
        
        int RLS_Threshold = 30;
        /*
        this.imgRLS = ImageUtils.executeRLSA(this.imgOtsuBin, RLS_Threshold, RunLengthSmoothingKind.HORIZONTAL);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_r_h.png"), this.imgRLS, "png");
        }
        
        this.imgRLS = ImageUtils.executeRLSA(this.imgOtsuBin, RLS_Threshold, RunLengthSmoothingKind.VERTICAL);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_r_v.png"), this.imgRLS, "png");
        }
        
        this.imgRLS = ImageUtils.executeRLSA(this.imgOtsuBin, RLS_Threshold, RunLengthSmoothingKind.BOTH_OR);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_r_hov.png"), this.imgRLS, "png");
        }
        */
        
        this.imgRLS = ImageUtils.executeRLSA(this.imgOtsuBin, RLS_Threshold, RunLengthSmoothingKind.BOTH_AND);
        if (this.isStoreTempFiles) {
            writebw(new File(this.fileName + "_r_hav.png"), this.imgRLS, "png");
        }
        
        Rectangle rectangle = null;
        
        this.diffRect = calculateBoundingBoxWithDifferenceCount(this.imgRLS);
        if ((this.diffRect.x > -1) && (this.diffRect.y > -1) && (this.diffRect.width < originWidth) && (this.diffRect.height < originHeight) && !isCropAreaTooSmall(this.diffRect, originWidth, originHeight) && !isCropAreaTooLarge(this.diffRect, originWidth, originHeight)) {
            isSuccess = true;
            rectangle = this.diffRect;
        }
        
        /*
        else {
            this.projRect = calculateBoundingBoxWithProjection(imgRLSA_HAV);
            if ((this.projRect.x > -1) && (this.projRect.y > -1) && (this.projRect.width < originWidth) && (this.projRect.height < originHeight) && !isCropAreaTooSmall(this.projRect, originWidth, originHeight) && !isCropAreaTooLarge(this.projRect, originWidth, originHeight)) {
                isSuccess = true;
                rectangle = this.projRect;
            }
            else {
                this.rect = calculateBoundingBoxWithEdgeDetection(buffImgBL);
                if ((this.rect.x > -1) && (this.rect.y > -1) && (this.rect.width < originWidth) && (this.rect.height < originHeight) && !isCropAreaTooSmall(this.rect, originWidth, originHeight) && !isCropAreaTooLarge(this.rect, originWidth, originHeight)) {
                    isSuccess = true;
                    rectangle = this.rect;
                }
            }
        }
        
        
        this.rect = calculateBoundingBoxWithEdgeDetection(this.imgGray);
        if ((this.rect.x > -1) && (this.diffRect.y > -1) && (this.rect.width < originWidth) && (this.rect.height < originHeight) && !isCropAreaTooSmall(this.rect, originWidth, originHeight) && !isCropAreaTooLarge(this.rect, originWidth, originHeight)) {
            isSuccess = true;
            rectangle = this.rect;
        }
        else {
            this.diffRect = calculateBoundingBoxWithDifferenceCount(buffImgBL);
            if ((this.diffRect.x > -1) && (this.diffRect.y > -1) && (this.diffRect.width < originWidth) && (this.diffRect.height < originHeight) && !isCropAreaTooSmall(this.diffRect, originWidth, originHeight) && !isCropAreaTooLarge(this.diffRect, originWidth, originHeight)) {
                isSuccess = true;
                rectangle = this.diffRect;
            }
            else {
                this.projRect = calculateBoundingBoxWithProjection(buffImgBL);
                if ((this.projRect.x > -1) && (this.projRect.y > -1) && (this.projRect.width < originWidth) && (this.projRect.height < originHeight) && !isCropAreaTooSmall(this.projRect, originWidth, originHeight) && !isCropAreaTooLarge(this.projRect, originWidth, originHeight)) {
                    isSuccess = true;
                    rectangle = this.projRect;
                }
            }
        }
        */
        
        // ?HarrisFast.computeCorner(this.imgGray);
        if (isSuccess) {
            try {
                rectangle = adjustRectangle(rectangle, originWidth, originHeight, 30);
                this.img = cropSubimage(this.img, rectangle);
            } catch (Exception e) {
                System.out.println("Can't crop! " + this.fileName + " x " + rectangle.x  + " y " + rectangle.y + " w " + rectangle.width + " h " + rectangle.height);
                System.err.println(e.getMessage());
                isSuccess = false;
            }
        }   
        else {
            if (this.imgReduced != null) {
                writebw(new File(this.fileName + "_r.png"), this.imgReduced, "png");
            }
            if (this.imgOtsuBin != null) {
                writebw(new File(this.fileName + "_ob.png"), this.imgOtsuBin, "png");
            }
            if (this.imgRLS != null) {
                writebw(new File(this.fileName + "_rls.png"), this.imgRLS, "png");
            }
        }
        return isSuccess;
    }

}
