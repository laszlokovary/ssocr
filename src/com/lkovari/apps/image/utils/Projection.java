/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: Projection.java
 * Created: Nov 6, 2013 12:24:48 PM
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Projection {


    /**
     * 
     * Method: calculateHorizontalProjectionFromBinImg 
     * @param buffImgIn BufferedImage
     * @param imgCc int most common color of image in RGB
     * @return int[] horizontal projection values
     */
    public static int[] calculateHorizontalProjectionFromBinImg(BufferedImage buffImgIn, int imgCc) {
        int width = buffImgIn.getWidth();
        int height = buffImgIn.getHeight();
        int[] horizontalProjection = new int[height];
        int startOfProjection = imgCc;
        for (int y=0; y < height; y++) {
            int colorChangeCnt = 0;
            for (int x=0; x < width; x++) {
                if (startOfProjection != buffImgIn.getRGB(x, y)) {
                    startOfProjection = buffImgIn.getRGB(x, y);
                    colorChangeCnt++;
                }
            }
            horizontalProjection[y] = colorChangeCnt;
        }
        return horizontalProjection;
    }

    /**
     * 
     * Method: calculateVerticalProjectionFromBinImg 
     * @param buffImgIn BufferedImage
     * @param imgCc int most common color of image in RGB
     * @return int[] vertical projection values
     */
    public static int[] calculateVerticalProjectionFromBinImg(BufferedImage buffImgIn, int imgCc) {
        int width = buffImgIn.getWidth();
        int height = buffImgIn.getHeight();
        int[] verticalProjection = new int[width];        
        int startOfProjection = imgCc;
        for (int x=0; x < width; x++) {
            int colorChangeCnt = 0;
            for (int y=0; y < height; y++) {
                if (startOfProjection != buffImgIn.getRGB(x, y)) {
                    startOfProjection = buffImgIn.getRGB(x, y);
                    colorChangeCnt++;
                }
            }
            verticalProjection[x] = colorChangeCnt;
        }
        return verticalProjection;
    }
    
    
    /**
     * 
     * Method: calculateHorizontalProjection 
     * @param buffImgIn
     * Capture color changes horizontally
     * @return
     */
    public static int[] calculateHorizontalProjectionFromColorImg(BufferedImage buffImgIn) {
        BufferedImage buffImgBinIn = OtsuBinarize.convertToBinary(buffImgIn, 0);
        Color mostCommonColor = ImageUtils.detectMostCommonColor(buffImgBinIn);
        return calculateHorizontalProjectionFromBinImg(buffImgBinIn, mostCommonColor.getRGB());
    }
    
    /**
     * 
     * Method: calculateVerticalProjection 
     * @param buffImgIn
     * Capture color changes vertically
     * @return
     */
    public static int[] calculateVerticalProjectionFromColorImg(BufferedImage buffImgIn) {
        BufferedImage buffImgBinIn = OtsuBinarize.convertToBinary(buffImgIn, 0);
        Color mostCommonColor = ImageUtils.detectMostCommonColor(buffImgBinIn);
        return calculateVerticalProjectionFromBinImg(buffImgBinIn, mostCommonColor.getRGB());
    }
    
    /**
     * 
     * Method: calculateHorizontalPeakRow 
     * @param horizontalValues
     * @return
     */
    public static int calculateHorizontalPeakRow(int[] horizontalValues) {
        int peakh = Integer.MIN_VALUE;
        int cnth = 0;
        int peakHIx = -1;
        for (int chg : horizontalValues) {
            if (peakh < chg) {
                peakh = chg;
                peakHIx = cnth;
            }
            cnth++;
        }
        return peakHIx;
    }
    
    /**
     * 
     * Method: calculateVerticalPeakRow 
     * @param verticalValues
     * @return
     */
    public static int calculateVerticalPeakRow(int[] verticalValues) {
        int peakv = Integer.MIN_VALUE;
        int cntv = 0;
        int peakVIx = -1;
        for (int chg : verticalValues) {
            if (peakv < chg) {
                peakv = chg;
                peakVIx = cntv;
            }
            cntv++;
        }
        return peakVIx;
    }

    /**
     * 
     * Method: calculateHorizontalPeakValue 
     * @param horizontalValues
     * @return
     */
    public static int calculateHorizontalPeakValue(int[] horizontalValues) {
        int peakh = Integer.MIN_VALUE;
        for (int chg : horizontalValues) {
            if (peakh < chg) {
                peakh = chg;
            }
        }
        return peakh;
    }
    
    /**
     * 
     * Method: calculateVerticalPeakValue 
     * @param verticalValues
     * @return
     */
    public static int calculateVerticalPeakValue(int[] verticalValues) {
        int peakv = Integer.MIN_VALUE;
        for (int chg : verticalValues) {
            if (peakv < chg) {
                peakv = chg;
            }
        }
        return peakv;
    }
    
    /**
     * 
     * Method: calculatePeakByThreahold 
     * @param projectionValues
     * @param threshold
     * @param isFromBegin
     * @return
     */
    public static int calculatePeakByThreahold(int[] projectionValues, int threshold, boolean isFromBegin) {
        int peak = -1;
        int size = projectionValues.length;
        if (isFromBegin) {
            for (int ix = 0; ix < size; ix++) {
                int v = projectionValues[ix];
                if (v > threshold) {
                    peak = ix;
                    break;
                }
            }
        }
        else {
            for (int ix = size-1; ix >= 0; ix--) {
                int v = projectionValues[ix];
                if (v > threshold) {
                    peak = ix;
                    break;
                }
            }
        }
        return peak;
    }

    /**
     * 
     * Method: calculateLeftTopPositionByThreahold 
     * @param horizontalProjection
     * @param verticalProjection
     * @param threshold
     * @return
     */
    public static Point calculateLeftTopProjectionPositionByThreahold(int[] horizontalProjection, int[] verticalProjection, int threshold) {
        Point ltPoint = new Point();
        int leftPos = calculatePeakByThreahold(verticalProjection, threshold, true);
        int topPos = calculatePeakByThreahold(horizontalProjection, threshold, true);
        ltPoint.x = leftPos;
        ltPoint.y = topPos;
        return ltPoint;
    }

    /**
     * 
     * Method: calculateRightBottomPositionByThreahold 
     * @param horizontalProjection
     * @param verticalProjection
     * @param threshold
     * @return
     */
    public static Point calculateRightBottomProjectionPositionByThreahold(int[] horizontalProjection, int[] verticalProjection, int threshold) {
        Point rbPoint = new Point();
        int rightPos = calculatePeakByThreahold(verticalProjection, threshold, false);
        int bottomPos = calculatePeakByThreahold(horizontalProjection, threshold, false);
        rbPoint.x = rightPos;
        rbPoint.y = bottomPos;
        return rbPoint;
    }

    /**
     * 
     * Method: calculateHorizontalDifferenceSumFromBinImg 
     * @param buffImgIn
     * @param imgCc
     * @return
     */
    public static int[] calculateHorizontalDifferenceSumFromBinImg(BufferedImage buffImgIn, int imgCc) {
        int width = buffImgIn.getWidth();
        int height = buffImgIn.getHeight();
        int[] horizontalProjection = new int[height];
        for (int y=0; y < height; y++) {
            int colorDiffCnt = 0;
            for (int x=0; x < width; x++) {
                if (imgCc != buffImgIn.getRGB(x, y)) {
                    colorDiffCnt++;
                }
            }
            horizontalProjection[y] = colorDiffCnt;
        }
        return horizontalProjection;
    }

    /**
     * 
     * Method: calculateVerticalDifferenceSumFromBinImg 
     * @param buffImgIn
     * @param imgCc
     * @return
     */
    public static int[] calculateVerticalDifferenceSumFromBinImg(BufferedImage buffImgIn, int imgCc) {
        int width = buffImgIn.getWidth();
        int height = buffImgIn.getHeight();
        int[] verticalProjection = new int[width];        
        for (int x=0; x < width; x++) {
            int colorDiffCnt = 0;
            for (int y=0; y < height; y++) {
                if (imgCc != buffImgIn.getRGB(x, y)) {
                    colorDiffCnt++;
                }
            }
            verticalProjection[x] = colorDiffCnt;
        }
        return verticalProjection;
    }

    /**
     * 
     * Method: calculateLeftTopDiffPositionByThreahold 
     * @param horizontalDifferences
     * @param verticalDifferences
     * @param threshold
     * @return
     */
    public static Point calculateLeftTopDiffPositionByThreahold(int[] horizontalDifferences, int[] verticalDifferences, int threshold) {
        Point ltPoint = new Point();
        int leftPos = calculatePeakByThreahold(verticalDifferences, threshold, true);
        int rightPos = calculatePeakByThreahold(horizontalDifferences, threshold, true);
        ltPoint.x = leftPos;
        ltPoint.y = rightPos;
        return ltPoint;
    }

    /**
     * 
     * Method: calculateRightBottomDiffPositionByThreahold 
     * @param horizontalDifferences
     * @param verticalDifferences
     * @param threshold
     * @return
     */
    public static Point calculateRightBottomDiffPositionByThreahold(int[] horizontalDifferences, int[] verticalDifferences, int threshold) {
        Point rbPoint = new Point();
        int rightPos = calculatePeakByThreahold(verticalDifferences, threshold, false);
        int bottomPos = calculatePeakByThreahold(horizontalDifferences, threshold, false);
        rbPoint.x = rightPos;
        rbPoint.y = bottomPos;
        return rbPoint;
    }
 
    public static Rectangle calculateBorderlessRectangle(int[] horizontalDifferences, int[] verticalDifferences) {
        Rectangle rect = new Rectangle();
        
        return rect;
    }
}
