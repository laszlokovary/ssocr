/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: ImgOpt.java
 * Created: 2013.10.25. 21:54:10
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ImgOpt {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        System.out.println("C  " + c.getTimeInMillis());
        List<String> testFiles = new ArrayList<String>();
        int cnt = 0;
        long sumElapsed = 0;
//        testFiles.add("F://img//img_6656");        
//        testFiles.add("F:\\img\\img_6656");
        /*
        testFiles.add("F://img//img_3078");
        testFiles.add("F://img//img_6638");        
        testFiles.add("F://img//img_6650");        
        testFiles.add("F://img//img_3077");        
        testFiles.add("F:\\img\\img_6651");
        testFiles.add("F:\\img\\img_6655");
        testFiles.add("F:\\img\\img_6654");
        testFiles.add("F:\\img\\img_6653");
        testFiles.add("F:\\img\\img_6652");
        testFiles.add("F:\\img\\img_2075");
        testFiles.add("F:\\img\\img_6650");
        testFiles.add("F:\\img\\img_2074");
        testFiles.add("F:\\img\\img_2497");
        testFiles.add("F://img//img_5709");
        testFiles.add("F://img//img_4734");

        testFiles.add("F://img//img_3095");
        testFiles.add("F://img//img_2108");
        
        testFiles.add("F://img//img_3075");
        testFiles.add("F://img//img_3073");
        testFiles.add("F://img//img_3097");
        testFiles.add("F://img//img_2060");
        testFiles.add("F://img//img_2915");
        testFiles.add("F://img//img_2879");
        testFiles.add("F://img//img_3021");
        testFiles.add("F://img//img_3022");
        testFiles.add("F://img//img_3075");
        testFiles.add("F://img//img_2916");
        testFiles.add("F://img//img_2914");
        testFiles.add("F://img//img_2469");
        testFiles.add("F://img//img_2360");
        testFiles.add("F://img//img_6038");
        testFiles.add("F://img//img_4734");
        
        testFiles.add("F://img//img_3077");
        testFiles.add("F://img//img_3079");
        testFiles.add("F://img//img_3083");
        testFiles.add("F://img//img_2915");
        testFiles.add("F://img//img_3088");
        testFiles.add("F://img//img_3087");
        testFiles.add("F://img//img_3094");
        testFiles.add("F://img//img_3086");
        testFiles.add("F://img//img_3084");
        testFiles.add("F://img//img_3083");
        testFiles.add("F://img//img_3082");
        testFiles.add("F://img//img_3081");
        testFiles.add("F://img//img_3080");
        testFiles.add("F://img//img_3076");
        testFiles.add("F://img//img_3074");
        testFiles.add("F://img//img_3072");
        testFiles.add("F://img//img_3022");
        testFiles.add("F://img//img_3021");
        testFiles.add("F://img//img_2915");
        testFiles.add("F://img//img_2556");
        testFiles.add("F://img//img_248");
        testFiles.add("F://img//img_2477");
        testFiles.add("F://img//img_2060");
        testFiles.add("F://img//img_3085");
        testFiles.add("F://img//img_2072");
        testFiles.add("F://img//img_3083");
        testFiles.add("F://img//img_2067");
        testFiles.add("F://img//img_2063");
        testFiles.add("F://img//img_2879");
        testFiles.add("F://img//img_4375");
        testFiles.add("F://img//img_1275");
        testFiles.add("F://img//img_6600");
        testFiles.add("F://img//img_2071");
        */
        File imgDir = new File("F://img//");
        CropImg cropImg = new CropImg();
        if (imgDir.isDirectory() && imgDir.exists()) {
            for (File file : imgDir.listFiles()) {
                String fn = file.getAbsolutePath(); 
                int dotIx = fn.indexOf(".");
                String fnWoExt = fn.substring(0, dotIx);
                cropImg.readFile(fnWoExt);
                
                long elapsed = System.currentTimeMillis();
                try {
                    boolean isCropOk = cropImg.crop();
                    sumElapsed += (System.currentTimeMillis() - elapsed);
                    if (isCropOk) {
                        cropImg.write(new File(fnWoExt+"_crop.jpg"), "jpg");
                    }    
                    else {
                        System.err.println("Crop failed! " + fnWoExt);
                    }
                    cnt++;
                }
                catch (Exception e) {
                    System.err.println("Crop failed! " + fnWoExt + " " + e.getMessage());
                }
            }
        }
        
        /*
        CropImg cropImg = new CropImg();
        cnt = 0;
        for (String fn : testFiles) {
            cropImg.readFile(fn);
            if (cropImg.crop()) {
                long elapsed = System.currentTimeMillis();
                cropImg.write(new File(fn+"_crop.jpg"), "jpg");
                sumElapsed += (System.currentTimeMillis() - elapsed);
                cnt++;
            }    
            else {
                System.err.println(fn + " Crop failed!");
            }
        }
        */
        System.out.println(" " + cnt + " files done. " + sumElapsed + " mils, " + (sumElapsed / cnt) + " mils per image." );
    }

}
