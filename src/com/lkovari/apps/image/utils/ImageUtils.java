/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ImageUtils
 * Package: com.lkovari.imgage.utils.crop
 * File: ImageUtils.java
 * Created: Nov 6, 2013 12:31:31 PM
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.utils;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import com.lkovari.apps.image.utils.HarrisFast.Corner;



/**
 * 
 * @author lkovari
 * ImageUtils
 * 
 */
public class ImageUtils {
    public static int PROJECTION_THRESHOLD = 1;
    public static int DIFFERENCE_THRESHOLD = 1;
    public static int RESIZE_RATIO = 7;
    public static int THETA_VALUE = 20;
    public static int BORDER_AREA_PERCENT = 25;
    
    /**
     * 
     * Method: convertToBinaryWithOtsu 
     * @param buffImgIn
     * @return
     */
    public static BufferedImage convertToBinaryWithOtsu(BufferedImage buffImgIn, int thrasholdCorrection) {
        return OtsuBinarize.convertToBinary(buffImgIn, thrasholdCorrection);
    }
    
    /**
     * 
     * Method: createBufferedImageFrom 
     * @param buffImgIn
     * @param imageType
     * @return
     */
    private static BufferedImage createBufferedImageFrom(BufferedImage buffImgIn, int imageType) {
        int width = buffImgIn.getWidth();
        int height = buffImgIn.getHeight();
        BufferedImage buffImgOut = null;
        buffImgOut = new BufferedImage(width, height, imageType);
        final Graphics2D g2d = (Graphics2D) buffImgIn.getGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR); // or .._BICUBIC
        g2d.drawImage(buffImgIn, 0, 0, null);
        g2d.dispose();
        return buffImgOut;
    }
    
    /**
     * 
     * Method: convertToBlackAndWhite 
     * @param buffImgIn
     * @return
     */
    private BufferedImage convertToBlackAndWhite(BufferedImage buffImgIn) {
        BufferedImage buffImgOut = null;
        buffImgOut = createBufferedImageFrom(buffImgIn, BufferedImage.TYPE_BYTE_BINARY);
        return buffImgOut;
    }

    public static BufferedImage convertToGrayWithFilter(BufferedImage buffImgIn) {
        BufferedImage buffImgOut = null;
        buffImgOut = createBufferedImageFrom(buffImgIn, BufferedImage.TYPE_BYTE_GRAY);
        ColorConvertOp colorConvertOp = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        colorConvertOp.filter(buffImgIn, buffImgOut);        
        return buffImgOut;
    }
    
    /**
     * 
     * Method: convertToGray 
     * @param buffImgIn
     * @return
     */
    public static BufferedImage convertToGray_BYTE_GRAY(BufferedImage buffImgIn) {
        BufferedImage buffImgOut = null;
        buffImgOut = createBufferedImageFrom(buffImgIn, BufferedImage.TYPE_BYTE_GRAY);
        return buffImgOut;
    }

 // The average grayscale method
    public static BufferedImage convertToGrayScaleAvg(BufferedImage buffImgIn) {
        int alpha, red, green, blue;
        int newPixel;
     
        BufferedImage avg_gray = new BufferedImage(buffImgIn.getWidth(), buffImgIn.getHeight(), buffImgIn.getType());
        int[] avgLUT = new int[766];
        for(int i=0; i<avgLUT.length; i++) avgLUT[i] = (int) (i / 3);
     
        for(int i=0; i<buffImgIn.getWidth(); i++) {
            for(int j=0; j<buffImgIn.getHeight(); j++) {
                // Get pixels by R, G, B
                alpha = new Color(buffImgIn.getRGB(i, j)).getAlpha();
                red = new Color(buffImgIn.getRGB(i, j)).getRed();
                green = new Color(buffImgIn.getRGB(i, j)).getGreen();
                blue = new Color(buffImgIn.getRGB(i, j)).getBlue();
                
                newPixel = red + green + blue;
                newPixel = avgLUT[newPixel];
                // Return back to original format
                newPixel = colorToRGB(alpha, newPixel, newPixel, newPixel);
     
                // Write pixels into image
                avg_gray.setRGB(i, j, newPixel);
     
            }
        }
        return avg_gray;
    }    
    
    public static BufferedImage closingImage(BufferedImage buffImgIn, StructElementShape shape, int size) {
        Closing closing = new Closing(shape, size);
        BufferedImage closingImage = closing.execute(buffImgIn);
        closing = null;
        return closingImage;
    }

    public static BufferedImage openingImage(BufferedImage buffImgIn, StructElementShape shape, int size) {
        BufferedImage img = null;
        Opening opening = new Opening(shape, size);
        BufferedImage openingImage = opening.execute(buffImgIn);
        opening = null;
        return openingImage;
    }
    
    
    /**
     * 
     * Method: convertToGrayScale 
     * @param buffImgIn
     * @return
     */
    public static BufferedImage convertToGrayScaleNO_BYTE_GRAY(BufferedImage buffImgIn) {
        int alpha, red, green, blue;
        int newPixel;
        BufferedImage lum = new BufferedImage(buffImgIn.getWidth(), buffImgIn.getHeight(), buffImgIn.getType());
        for (int i = 0; i < buffImgIn.getWidth(); i++) {
            for (int j = 0; j < buffImgIn.getHeight(); j++) {
                // Get pixels by R, G, B
                alpha = new Color(buffImgIn.getRGB(i, j)).getAlpha();
                red = new Color(buffImgIn.getRGB(i, j)).getRed();
                green = new Color(buffImgIn.getRGB(i, j)).getGreen();
                blue = new Color(buffImgIn.getRGB(i, j)).getBlue();
                red = (int) (0.21 * red + 0.71 * green + 0.07 * blue);
                // Return back to original format
                newPixel = colorToRGB(alpha, red, red, red);
                // Write pixels into image
                lum.setRGB(i, j, newPixel);
            }
        }
        return lum;
    } 
    
    
    /**
     * 
     * Method: detectMostCommonColor 
     * @param img
     * @return
     */
    public static Color detectMostCommonColor(BufferedImage img) {
        Map<Color, Integer> colorsMap = new HashMap<Color, Integer>();
        int width = img.getWidth();
        int height = img.getHeight();
        for(int y = height - 1; y >= 0; y--) {
            for(int x = width - 1; x >= 0; x--) {               
                Color c = new Color(img.getRGB(x, y));
                boolean isFound = colorsMap.containsKey(c);
                if (!isFound) {
                    colorsMap.put(c, new Integer(1));
                }
                else {
                    colorsMap.put(c, colorsMap.get(c) + 1);
                }
            }
        }
        List list = new LinkedList(colorsMap.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
            }
        });    
        Color mostCommonColor = null;
        Map.Entry mcc = (Map.Entry )list.get(list.size()-1);
        mostCommonColor = (Color) mcc.getKey();
        list.clear();
        list = null;
        colorsMap.clear();
        colorsMap = null;
        return mostCommonColor;
    }

    /**
     * 
     * Method: calculateAngle 
     * @param p1
     * @param p2
     * @return
     */
    public static double calculateAngle(Point p1, Point p2) {
        float xDiff = p2.x - p1.x; 
        float yDiff = p2.y - p1.y; 
        return Math.atan2(yDiff, xDiff) * (180 / Math.PI);
    }    
    
    
    public static Corner[] determinateConrer(BufferedImage buffImgIn) {
        Corner[] corners = null;
        corners = HarrisFast.computeCorner(buffImgIn);
        return corners;
    }

    /**
     * 
     * Method: resizeImage 
     * @param buffImgIn
     * @param width integer width
     * @param height integer height
     * @param interpolation Object           
     *  RenderingHints.VALUE_INTERPOLATION_BILINEAR
     *  RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR
     *  RenderingHints.VALUE_INTERPOLATION_BICUBIC
     * @return
     */
    public static BufferedImage resizeImage(BufferedImage buffImgIn, int width, int height, Object interpolation) {
        int originWidth = buffImgIn.getWidth();  
        int originHeight = buffImgIn.getHeight();  
        BufferedImage buffImgOut = new BufferedImage(width, height, buffImgIn.getType());  
        Graphics2D g = buffImgOut.createGraphics();  
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, interpolation);  
        g.drawImage(buffImgIn, 0, 0, width, height, 0, 0, originWidth, originHeight, null);
        g.dispose();  
        return buffImgOut;  
    }

    public static BufferedImage convertToBufferedImage(Image image) {
        if (image instanceof BufferedImage) {
            return (BufferedImage)image;
        }

        // This code ensures that all the pixels in the image are loaded
        image = new ImageIcon(image).getImage();

        // Create a buffered image with a format that's compatible with the screen
        BufferedImage bimage = null;
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
            // Determine the type of transparency of the new buffered image
            int transparency = Transparency.OPAQUE;

            // Create the buffered image
            GraphicsDevice gs = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gs.getDefaultConfiguration();
            bimage = gc.createCompatibleImage(image.getWidth(null), image.getHeight(null), transparency);
        } catch (HeadlessException e) {
            // The system does not have a screen
        }

        if (bimage == null) {
            // Create a buffered image using the default color model
            int type = BufferedImage.TYPE_INT_RGB;
            bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
        }

        // Copy image to buffered image
        Graphics g = bimage.createGraphics();

        // Paint the image onto the buffered image
        g.drawImage(image, 0, 0, null);
        g.dispose();

        return bimage;
    }
    
    public static BufferedImage resize(BufferedImage buffImgIn, int max_side, float quality) {
        double aspect_ratio = ((double)buffImgIn.getWidth()) / ((double)buffImgIn.getHeight());
        int width, height;

        if(buffImgIn.getWidth() >= buffImgIn.getHeight()) {
            width = max_side;
            height = (int)(((double)max_side) / aspect_ratio);
        }
        else {
            width = (int)(((double)max_side) * aspect_ratio);
            height = max_side;
        }

        Image scaled_img = buffImgIn.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage buffImgOut = convertToBufferedImage(scaled_img);
        return buffImgOut;
    }    
    
    /**
     * 
     * Method: rotateImage 
     * @param buffImgIn
     * @param angle
     * @return
     */
    public static BufferedImage rotateImage(BufferedImage buffImgIn, int angle) {
        int originWidth = buffImgIn.getWidth();  
        int originHeight = buffImgIn.getHeight();  
        BufferedImage buffImgOut = new BufferedImage(originWidth, originHeight, buffImgIn.getType());  
        Graphics2D g = buffImgOut.createGraphics();  
        g.rotate(Math.toRadians(angle), originWidth/2, originHeight/2);  
        g.drawImage(buffImgIn, null, 0, 0);  
        return buffImgOut;
    }
    
    /**
     * 
     * Method: isBlack 
     * @param image
     * @param x
     * @param y
     * @return
     */
    public static boolean isBlack(BufferedImage image, int x, int y) {

        if (image.getType() == BufferedImage.TYPE_BYTE_BINARY) {

            WritableRaster raster = image.getRaster();

            int pixelRGBValue = raster.getSample(x, y, 0);
            return pixelRGBValue == 0;
        }

        int luminanceValue = 140;
        return isBlack(image, x, y, luminanceValue);
    }

    /**
     * 
     * Method: isBlack 
     * @param image
     * @param x
     * @param y
     * @param luminanceCutOff
     * @return
     */
    public static boolean isBlack(BufferedImage image, int x, int y, int luminanceCutOff) {
        int pixelRGBValue;
        int r;
        int g;
        int b;
        double luminance = 0.0;

        // return white on areas outside of image boundaries
        if (x < 0 || y < 0 || x > image.getWidth() || y > image.getHeight()) {
            return false;
        }

        try {
            pixelRGBValue = image.getRGB(x, y);
            r = (pixelRGBValue >> 16) & 0xff;
            g = (pixelRGBValue >> 8) & 0xff;
            b = (pixelRGBValue >> 0) & 0xff;
            luminance = (r * 0.299) + (g * 0.587) + (b * 0.114);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (luminance < luminanceCutOff);
    }
    
    /**
     * 
     * Method: rotate 
     * @param image
     * @param angle
     * @param cx
     * @param cy
     * @return
     */
    public static BufferedImage rotate(BufferedImage image, double angle,
                                       int cx, int cy) {
        int width = image.getWidth(null);
        int height = image.getHeight(null);

        int minX = 0;
        int minY = 0;
        int maxX = 0;
        int maxY = 0;

        int[] corners = {0, 0, width, 0, width, height, 0, height};

        double theta = Math.toRadians(angle);
        for (int i = 0; i < corners.length; i += 2) {
            int x = (int) (Math.cos(theta) * (corners[i] - cx)
                    - Math.sin(theta) * (corners[i + 1] - cy) + cx);
            int y = (int) (Math.sin(theta) * (corners[i] - cx)
                    + Math.cos(theta) * (corners[i + 1] - cy) + cy);

            if (x > maxX) {
                maxX = x;
            }

            if (x < minX) {
                minX = x;
            }

            if (y > maxY) {
                maxY = y;
            }

            if (y < minY) {
                minY = y;
            }

        }

        cx = (cx - minX); // NOSONAR
        cy = (cy - minY); // NOSONAR

        BufferedImage bi = new BufferedImage((maxX - minX), (maxY - minY),
                image.getType());
        Graphics2D g2 = bi.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BICUBIC);

        g2.setBackground(Color.white);
        g2.fillRect(0, 0, bi.getWidth(), bi.getHeight());

        AffineTransform at = new AffineTransform();
        at.rotate(theta, cx, cy);

        g2.setTransform(at);
        g2.drawImage(image, -minX, -minY, null);
        g2.dispose();

        return bi;

    }
    
    /**
     * 
     * Method: changeToTransparentByColor 
     * @param buffImgIn
     * @param color
     * @return
     */
    public static BufferedImage changeToTransparentByColor(BufferedImage buffImgIn, Color color) {
        BufferedImage buffImgOut = new BufferedImage(buffImgIn.getWidth(), buffImgIn.getHeight(), BufferedImage.TYPE_INT_ARGB);  
        Graphics2D g = buffImgOut.createGraphics();  
        g.setComposite(AlphaComposite.Src);  
        g.drawImage(buffImgIn, null, 0, 0);  
        g.dispose();  
        for(int i = 0; i < buffImgOut.getHeight(); i++) {  
            for(int j = 0; j < buffImgOut.getWidth(); j++) {  
                if(buffImgOut.getRGB(j, i) == color.getRGB()) {  
                    buffImgOut.setRGB(j, i, 0x8F1C1C);  
                }  
            }  
        }  
        return buffImgOut;  
    }      

    /**
     * 
     * Method: colorToRGB 
     * @param alpha
     * @param red
     * @param green
     * @param blue
     * @return
     */
    private static int colorToRGB(int alpha, int red, int green, int blue) {
        int newPixel = 0;
        newPixel += alpha;
        newPixel = newPixel << 8;
        newPixel += red;
        newPixel = newPixel << 8;
        newPixel += green;
        newPixel = newPixel << 8;
        newPixel += blue;
        return newPixel;
    }
    

    public static Rectangle calculateBorderlessRectangle(int[] horizontalDifferences, int[] verticalDifferences) {
        Rectangle rect = new Rectangle();
        
        return rect;
    }
    
    /**
     * 
     * Method: isFoundBorder 
     * @param differenceValues
     * @param isFromBegin
     * @param threshold
     * @return
     */
    private static boolean isFoundBorder(int[] differenceValues, boolean isFromBegin, int peakValue) {
        boolean isFound = false;
        int size = differenceValues.length;
        int borderArea = (size / 100) * BORDER_AREA_PERCENT;
        int percent65OfPeakValue = (peakValue / 100) * 65;
        int percent10OfPeakValue = (peakValue / 100) * 10;
        int sum = 0;
        int avg = 0;
        if (isFromBegin) {
            int firstValue = differenceValues[0];
            if (firstValue > percent65OfPeakValue) {
                for (int ix = 0; ix < borderArea ; ix++) {
                    int v = differenceValues[ix];
                    sum += v;
                    if (avg > 0) {
                        int avgPercent = (avg /100) * 5;
                        if (v < avgPercent) {
                            isFound = true;
                            break;
                        }
                    }
                    avg = sum / (ix + 1);
                }
            }
        }
        else {
            int firstValue = differenceValues[size - 1];
            if (firstValue > percent65OfPeakValue) {
                for (int ix = size - 1; ix > borderArea; ix--) {
                    int v = differenceValues[ix];
                    sum += v;
                    if (avg > 0) {
                        int avgPercent = (avg /100) * 10;
                        if (v < avgPercent) {
                            isFound = true;
                            break;
                        }
                    }
                    avg = sum / (size - ix);
                }
            }
        }
        return isFound;
    }

    /**
     * 
     * Method: calculateBorderEnd 
     * @param differenceValues
     * @param isFromBegin
     * @param threshold
     * @return
     */
    private static int calculateBorderEnd(int[] differenceValues, boolean isFromBegin, int peakValue) {
        int borderEndIx = 0;
        int size = differenceValues.length;
        int borderArea = (size / 100) * BORDER_AREA_PERCENT;
        int threshold = (peakValue / 100) * 5;
        if (isFromBegin) {
            borderEndIx = 0;
            for (int ix = 0; ix < borderArea ; ix++) {
                int v = differenceValues[ix];
                if (v < threshold) {
                    borderEndIx = ix;
                    break;
                }
            }
        }
        else {
            for (int ix = size - 1; ix > borderArea; ix--) {
                int v = differenceValues[ix];
                if (v < threshold) {
                    borderEndIx = ix;
                    break;
                }
            }
        }
        return borderEndIx;
    }
    
    public static BufferedImage removeBorders(BufferedImage buffImgIn) {
        BufferedImage buffImgOut = null;
        BufferedImage imgOtsuBin = OtsuBinarize.convertToBinary(buffImgIn, -3);
        Color binCommonColor = detectMostCommonColor(buffImgIn);
        
        // calculate the non background pixels of each row
        int[] horizontalDifferences = Projection.calculateHorizontalDifferenceSumFromBinImg(imgOtsuBin, binCommonColor.getRGB());
        // calculate the non background pixels of each column
        int[] verticalDifferences = Projection.calculateVerticalDifferenceSumFromBinImg(imgOtsuBin, binCommonColor.getRGB());
        Point diffLTPoint = Projection.calculateLeftTopDiffPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.DIFFERENCE_THRESHOLD);
        Point diffRBPoint = Projection.calculateRightBottomDiffPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.DIFFERENCE_THRESHOLD);
        // maybe peak value represent the longest row
        int horizontalPeakValue = Projection.calculateHorizontalPeakValue(horizontalDifferences);
        int verticalPeakValue = Projection.calculateVerticalPeakValue(verticalDifferences);
        
        boolean isHorizontalBorderFoundFromBegin  = isFoundBorder(horizontalDifferences, true, horizontalPeakValue);
        boolean isHorizontalBorderFoundFromEnd  = isFoundBorder(horizontalDifferences, false, horizontalPeakValue);
        boolean isVerticalBorderFoundFromBegin  = isFoundBorder(verticalDifferences, true, verticalPeakValue);
        boolean isVerticalBorderFoundFromEnd  = isFoundBorder(verticalDifferences, false, verticalPeakValue);

        int left = 0;
        int top = 0;
        int right = buffImgIn.getWidth();
        int bottom = buffImgIn.getHeight();
        if (isHorizontalBorderFoundFromBegin) {
            top = calculateBorderEnd(horizontalDifferences, true, horizontalPeakValue);
        }
        if (isVerticalBorderFoundFromBegin) {
            left = calculateBorderEnd(verticalDifferences, true, verticalPeakValue);
        }
        if (isHorizontalBorderFoundFromEnd) {
            bottom = calculateBorderEnd(horizontalDifferences, false, horizontalPeakValue);
        }
        if (isVerticalBorderFoundFromEnd) {
            right = calculateBorderEnd(verticalDifferences, false, verticalPeakValue);
        }

        Rectangle rect = new Rectangle();
        rect.x = left;
        rect.y = top;
        rect.width = (right - left);
        rect.height = (bottom - top);
        BufferedImage subImage = buffImgIn.getSubimage(rect.x, rect.y, rect.width, rect.height);
        buffImgOut = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = buffImgOut.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.drawImage(subImage, 0, 0, null);
        g2d.dispose();
        return buffImgOut;
    }

    /**
     * 
     * Method: executeRLSA 
     * @param buffImgIn
     * @param threshold
     * @param rlsKind
     * @return
     */
    public static BufferedImage executeRLSA(BufferedImage buffImgIn, int threshold, RunLengthSmoothingKind rlsKind) {
        return RunLengthSmoothing.execute(buffImgIn, threshold, rlsKind);
    }

    /**
     * 
     * Method: medianFilter 
     * @param buffImgIn
     * @param s
     * @return
     */
    public static BufferedImage medianFilter(BufferedImage buffImgIn, int s) {
        MedianFilter medianFilter = null;
        BufferedImage buffImgOut = new BufferedImage(buffImgIn.getWidth(), buffImgIn.getHeight(), buffImgIn.getType());
        try {
            medianFilter = new MedianFilter(s);
            medianFilter.filter(buffImgIn, buffImgOut);
        }
        finally {
            medianFilter = null;
        }
        return buffImgOut;
    }
    
    public static BufferedImage copyImageDeep(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    public static BufferedImage drawBorderOnTheImage(BufferedImage buffImgIn, Rectangle rect, Color c) {
        BufferedImage buffImgOut = copyImageDeep(buffImgIn);
        Graphics graphics = buffImgOut.getGraphics();
        graphics.setColor(c);
        int x1 = rect.x;
        int y1 = rect.y;
        int x2 = rect.x + rect.width - 1;
        int y2 = rect.y;
        graphics.drawLine(x1, y1, x2, y2);

        x1 = rect.x + rect.width - 1;
        y1 = rect.y;
        x2 = rect.x + rect.width - 1;
        y2 = rect.y + rect.height - 1;
        graphics.drawLine(x1, y1, x2, y2);
        
        x1 = rect.x + rect.width - 1;
        y1 = rect.y + rect.height - 1;
        x2 = rect.x;
        y2 = rect.y + rect.height - 1;
        graphics.drawLine(x1, y1, x2, y2);
        
        x1 = rect.x;
        y1 = rect.y + rect.height - 1;
        x2 = rect.x;
        y2 = rect.y;
        graphics.drawLine(x1, y1, x2, y2);
        
        
        return buffImgOut;
    }
    
    public static BufferedImage drawBorderOnTheImage(BufferedImage buffImgIn, Color c) {
        BufferedImage buffImgOut = copyImageDeep(buffImgIn);
        Graphics graphics = buffImgOut.getGraphics();
        graphics.setColor(c);
        int x1 = buffImgIn.getMinX();
        int y1 = buffImgIn.getMinY();
        int x2 = buffImgIn.getMinX() + buffImgIn.getWidth() - 1;
        int y2 = buffImgIn.getMinY();
        graphics.drawLine(x1, y1, x2, y2);

        x1 = buffImgIn.getMinX() + buffImgIn.getWidth() - 1;
        y1 = buffImgIn.getMinY();
        x2 = buffImgIn.getMinX() + buffImgIn.getWidth() - 1;
        y2 = buffImgIn.getMinY() + buffImgIn.getHeight() - 1;
        graphics.drawLine(x1, y1, x2, y2);
        
        x1 = buffImgIn.getMinX() + buffImgIn.getWidth() - 1;
        y1 = buffImgIn.getMinY() + buffImgIn.getHeight() - 1;
        x2 = buffImgIn.getMinX();
        y2 = buffImgIn.getMinY() + buffImgIn.getHeight() - 1;
        graphics.drawLine(x1, y1, x2, y2);
        
        x1 = buffImgIn.getMinX();
        y1 = buffImgIn.getMinY() + buffImgIn.getHeight() - 1;
        x2 = buffImgIn.getMinX();
        y2 = buffImgIn.getMinY();
        graphics.drawLine(x1, y1, x2, y2);
        
        
        return buffImgOut;
    }

    
    public static BufferedImage modifyImageBrightness(BufferedImage buffImgIn, float thresholdMax, float brMax) {
        int alpha, red, green, blue;
        int newPixel;
     
        BufferedImage buffImgOut = new BufferedImage(buffImgIn.getWidth(), buffImgIn.getHeight(), buffImgIn.getType());
     
        for(int i=0; i<buffImgIn.getWidth(); i++) {
            for(int j=0; j<buffImgIn.getHeight(); j++) {
                // Get pixels by R, G, B
                alpha = new Color(buffImgIn.getRGB(i, j)).getAlpha();
                red = new Color(buffImgIn.getRGB(i, j)).getRed();
                green = new Color(buffImgIn.getRGB(i, j)).getGreen();
                blue = new Color(buffImgIn.getRGB(i, j)).getBlue();
                float[] colorHSB = new float[3]; 
                Color.RGBtoHSB(red, green, blue, colorHSB);
                
                if (colorHSB[2] > thresholdMax) {
                  colorHSB[2] += brMax;
                }
                
                newPixel = Color.HSBtoRGB(colorHSB[0], colorHSB[1],  colorHSB[2]);
                // Write pixels into image
                buffImgOut.setRGB(i, j, newPixel);
     
            }
        }
        return buffImgOut;
    }    

    
    public static int[] calculateHistogramData(BufferedImage buffImgIn) {
        int[] hdata = new int[256];
        int alpha, red, green, blue;
        int minGray = Integer.MAX_VALUE;
        int maxGray = Integer.MIN_VALUE;
        for (int ix=0; ix < buffImgIn.getWidth(); ix++) {
            for (int iy=0; iy < buffImgIn.getHeight(); iy++) {
                int color = buffImgIn.getRGB(ix, iy);

                // extract each color component
                red   = (color >>> 16) & 0xFF;
                green = (color >>>  8) & 0xFF;
                blue  = (color >>>  0) & 0xFF;

                // calc luminance in range 0.0 to 1.0; using SRGB luminance constants
                float luminance = (red * 0.2126f + green * 0.7152f + blue * 0.0722f);
                int grayScale = (int) luminance;
                if (minGray > grayScale)
                    minGray = grayScale;
                if (maxGray < grayScale)
                    maxGray = grayScale;
                
                hdata[grayScale]++;
                /*
                float[] inputHSB = new float[3];
                inputHSB = Color.RGBtoHSB(red, green, blue, inputHSB);
                // END: Takes too much time 
                float brightness = inputHSB[2];
                */
             }
        }
        System.out.println("Min " + minGray + " Max " + maxGray);
        return hdata;
        
    }
}
