/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ssocr
 * Package: com.lkovari.image.ssocr
 * File: SSOcrTest.java
 * Created: 2015 m�j. 23 18:18:25
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.ssocr;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.lkovari.apps.image.utils.CropImg;

public class SSOcrTest {

    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        System.out.println("C  " + c.getTimeInMillis());
        List<String> testFiles = new ArrayList<String>();
        int cnt = 0;
        long sumElapsed = 0;
        /*
        testFiles.add("F://img//ssocr_0");
        testFiles.add("F://img//ssocr00");        
        testFiles.add("F://img//ssocr01");        
        testFiles.add("F://img//ssocr02");        
        testFiles.add("F://img//ssocr03");        
        testFiles.add("F://img//ssocr04");        
        testFiles.add("F://img//ssocr05");        
        testFiles.add("F://img//ssocr06");
        */        
        testFiles.add("F://img//ssocr07");
        
        /*
        File imgDir = new File("F://img//");
        CropImg cropImg = new CropImg();
        if (imgDir.isDirectory() && imgDir.exists()) {
            for (File file : imgDir.listFiles()) {
                String fn = file.getAbsolutePath(); 
                int dotIx = fn.indexOf(".");
                String fnWoExt = fn.substring(0, dotIx);
                cropImg.readFile(fnWoExt);
                
                long elapsed = System.currentTimeMillis();
                try {
                    boolean isCropOk = cropImg.crop();
                    sumElapsed += (System.currentTimeMillis() - elapsed);
                    if (isCropOk) {
                        cropImg.write(new File(fnWoExt+"_crop.jpg"), "jpg");
                    }    
                    else {
                        System.err.println("Crop failed! " + fnWoExt);
                    }
                    cnt++;
                }
                catch (Exception e) {
                    System.err.println("Crop failed! " + fnWoExt + " " + e.getMessage());
                }
            }
        }
        */
        
        SSOcr ssOcr = new SSOcr();
        cnt = 0;
        long mils = System.currentTimeMillis();
        for (String fn : testFiles) {
            ssOcr.readFile(fn);
            ssOcr.recognize();
            ssOcr.write(new File(fn+"_pr.jpg"), "jpg");
            cnt++;
        }
        
        System.out.println(" " + cnt + " files done. " + (System.currentTimeMillis() - mils) + " mils.");    }

}
