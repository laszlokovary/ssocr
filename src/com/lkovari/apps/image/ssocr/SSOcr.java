/**
 * 
 * Copyright 2005-2013 by L�szl� K�v�ri
 * laszlo.kovari@eklsofttrade.com
 * 
 * Project: ssocr
 * Package: com.lkovari.image.ssocr
 * File: SSOcr.java
 * Created: 2015 m�j. 23 18:29:35
 * Author: lkovari 
 * 
 * Description:
 * 
 * 
 */
package com.lkovari.apps.image.ssocr;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.lkovari.apps.image.utils.HistogramEqualization;
import com.lkovari.apps.image.utils.ImageUtils;
import com.lkovari.apps.image.utils.Projection;
import com.lkovari.apps.image.utils.RunLengthSmoothingKind;
import com.lkovari.apps.image.utils.StructElementShape;

public class SSOcr {
    private JFrame frame = new JFrame();
    private JList labelList = null;
    private DefaultListModel listModel = new DefaultListModel();
    private BufferedImage img;
    private BufferedImage imgSub;
    private BufferedImage imgInv;
    private BufferedImage imgbw = null;
    private BufferedImage imgGray = null;
    private BufferedImage imgHistogram = null;
    private BufferedImage imgBr = null; 
    private BufferedImage imgOtsuBin = null;
    private BufferedImage imgReduced = null;
    private BufferedImage imgOpened = null;
    private BufferedImage imgClosed = null;
    private BufferedImage imgMedian = null;
    private BufferedImage imgHist = null;
    private BufferedImage imgORRb = null;
    private BufferedImage imgORRbSub = null;
    private BufferedImage imgORRbMark = null;
    private BufferedImage imgRLS = null;

    private Color binCommonColor = null;
    private Color grayCommonColor = null;
    private Color bwCommonColor = null;
    
    private Map<Color, Integer> colors = new HashMap<Color, Integer>();
    
    private String fileName = null;
    
    private boolean isStoreTempFiles = true;
    
    private void initializeUI() {
        Dimension size = new Dimension(1024,768);
        this.frame.setPreferredSize(size);
        this.labelList = new JList();
        this.labelList.setModel(this.listModel);
        this.labelList.setSize(size);
        this.labelList.setVisibleRowCount(1);
        
        this.frame.add(new JScrollPane(this.labelList));
        
//        this.frame.setLocationRelativeTo(null);
        this.frame.setPreferredSize(size);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.pack();
        this.frame.setVisible(true);
    }


    private void showTextInList(String text) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                listModel.addElement(text);
            }
        });
    }
    
    private void showImageInList(BufferedImage image) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ImageIcon imageIcon = new ImageIcon(image); 
                listModel.addElement(imageIcon);
            }
        });
    }
    
    public SSOcr() {
        super();
        initializeUI();
    }
    
    /**
     * 
     * Method: write
     * 
     * @param f
     */
    public void write(File f, String type) {
        try {
            ImageIO.write(this.img, type, f);
        } catch (IOException e) {
            throw new RuntimeException("Can't write image", e);
        }
    }

    /**
     * 
     * Method: writebw
     * 
     * @param f
     */
    public void writebw(File f, BufferedImage img, String type) {
        try {
            ImageIO.write(img, type, f);
        } catch (IOException e) {
            throw new RuntimeException("Can't write image", e);
        }
    }
    
    /**
     * 
     * Method: readFile
     * 
     * @param fn
     */
    public void readFile(String fn) {
        try {
            this.fileName = fn;
            File input = new File(fn + ".jpg");
            img = ImageIO.read(input);
        } catch (IOException e) {
            throw new RuntimeException("Can't read imag e" + fn, e);
        }
    }
    
    /**
     * 
     * Method: detectMostCommonColor
     * 
     * @param img
     * @return
     */
    private Color detectMostCommonColor(BufferedImage buffImgIn) {
        int width = buffImgIn.getWidth();
        int height = buffImgIn.getHeight();
        for (int y = height - 1; y >= 0; y--) {
            for (int x = width - 1; x >= 0; x--) {
                Color c = new Color(buffImgIn.getRGB(x, y));
                boolean isFound = colors.containsKey(c);
                if (!isFound) {
                    colors.put(c, new Integer(1));
                } else {
                    colors.put(c, colors.get(c) + 1);
                }
            }
        }
        List list = new LinkedList(colors.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });
        Color mostCommonColor = null;
        Map.Entry mcc = (Map.Entry) list.get(list.size() - 1);
        mostCommonColor = (Color) mcc.getKey();
        list.clear();
        list = null;
        colors.clear();
        return mostCommonColor;
    }    
    
    /**
     * 
     * Method: calculateBoundingBoxWithProjection 
     * @param buffImg
     * @return
     */
    private Rectangle calculateBoundingBoxWithProjection(BufferedImage buffImg) {
        Rectangle rect = new Rectangle();
        this.binCommonColor = detectMostCommonColor(buffImg);
        
        int[] horizontalDifferences = Projection.calculateHorizontalProjectionFromBinImg(buffImg, this.binCommonColor.getRGB());
        int[] verticalDifferences = Projection.calculateVerticalProjectionFromBinImg(buffImg, this.binCommonColor.getRGB());
        Point projLTPoint = Projection.calculateLeftTopProjectionPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.PROJECTION_THRESHOLD);
        Point projRBPoint = Projection.calculateRightBottomDiffPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.PROJECTION_THRESHOLD);

       
        rect.x = projLTPoint.x;
        rect.y = projLTPoint.y;
        rect.width = projRBPoint.x - projLTPoint.x;
        rect.height = projRBPoint.y - projLTPoint.y;
        return rect;
    }

    /**
     * 
     * Method: calculateBoundingBoxWithDifferenceCount 
     * @param buffImg
     * @return
     */
    private Rectangle calculateBoundingBoxWithDifferenceCount(BufferedImage buffImgIn) {
        Rectangle rect = new Rectangle();
//        this.imgOtsuBin = OtsuBinarize.convertToBinary(buffImg);
        this.binCommonColor = detectMostCommonColor(buffImgIn);
        // tried the difference count methode 
        int[] horizontalDifferences = Projection.calculateHorizontalDifferenceSumFromBinImg(buffImgIn, this.binCommonColor.getRGB());
        int[] verticalDifferences = Projection.calculateVerticalDifferenceSumFromBinImg(buffImgIn, this.binCommonColor.getRGB());
        Point diffLTPoint = Projection.calculateLeftTopDiffPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.DIFFERENCE_THRESHOLD);
        Point diffRBPoint = Projection.calculateRightBottomDiffPositionByThreahold(horizontalDifferences, verticalDifferences, ImageUtils.DIFFERENCE_THRESHOLD);

        diffLTPoint.x *= ImageUtils.RESIZE_RATIO;
        diffLTPoint.y *= ImageUtils.RESIZE_RATIO;
        diffRBPoint.x *= ImageUtils.RESIZE_RATIO;
        diffRBPoint.y *= ImageUtils.RESIZE_RATIO;
        
        rect.x = diffLTPoint.x;
        rect.y = diffLTPoint.y;
        rect.width = diffRBPoint.x - diffLTPoint.x;
        rect.height = diffRBPoint.y - diffLTPoint.y;
        return rect;
    }
    
    
    /**
     * 
     * Method: cropSubimage 
     * @param img
     * @param rect
     * @return
     */
    private BufferedImage cropSubimage(BufferedImage img, Rectangle rect) {
        BufferedImage subImage = img.getSubimage(rect.x, rect.y, rect.width, rect.height);
        BufferedImage newImg = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = newImg.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.drawImage(subImage, 0, 0, null);
        this.img = newImg;
        g2d.dispose();
        return newImg;
    }    
    
    /**
     * 
     * Method: adjustRectangle 
     * @param rect
     * @param originWidth
     * @param originHeight
     * @param delta
     * @return
     */
    private Rectangle adjustRectangle(Rectangle rect, int originWidth, int originHeight, int delta) {
        
        if ((rect.width + delta) <= originWidth) {
            rect.width += delta; 
        }
        if ((rect.height + delta) <= originHeight) {
            rect.height += delta; 
        }
        if ((rect.x - delta) > 0) {
            rect.x -= delta; 
        }
        if ((rect.y - delta) > 0) {
            rect.y -= delta; 
        }
        return rect;
    }
    
    
    public BufferedImage invertImage(BufferedImage img) {
        for (int x = 0; x < img.getWidth(); x++) {
            for (int y = 0; y < img.getHeight(); y++) {
                int rgba = img.getRGB(x, y);
                Color col = new Color(rgba, true);
                col = new Color(255 - col.getRed(), 255 - col.getGreen(), 255 - col.getBlue());
                img.setRGB(x, y, col.getRGB());
            }
        }
        return img;
    }
    
    public Integer recognize() {
        Integer number= null;
        
        boolean isSuccess = false;
        int originWidth = img.getWidth();
        int originHeight = img.getHeight();
        int originArea = originWidth * originHeight;
        
        showTextInList("Origin Image");
        showImageInList(this.img);

        
        this.imgMedian = ImageUtils.medianFilter(this.img, 3);
        if (this.isStoreTempFiles) {
//            writebw(new File(this.fileName + "_2_r.png"), this.imgReduced, "png");
            showTextInList("After Median filter");
            showImageInList(this.imgMedian);
        }
        
        
        
        //this.imgGray = ImageUtils.convertToGrayScaleAvg(this.img);
        //this.imgGray = ImageUtils.convertToGrayWithFilter(this.img);
        //this.imgGray = ImageUtils.convertToGrayScaleNO_BYTE_GRAY(this.img);
        
        this.imgGray = ImageUtils.convertToGrayScaleAvg(this.imgMedian);
        showTextInList("After Gray");
        showImageInList(this.imgGray);
        writebw(new File(this.fileName + "_g.jpg"), this.imgGray, "jpg");
        
        
        int[] histogramData = ImageUtils.calculateHistogramData(imgGray);
        
        this.imgHistogram = new BufferedImage(imgGray.getWidth(), imgGray.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
        Graphics2D g2d = this.imgHistogram.createGraphics();
        g2d.setColor(Color.blue);
        int w = imgGray.getWidth() / 255;
        int maxGray = Integer.MIN_VALUE;
        for (int ix = 0; ix <= 255; ix++) {
            if (histogramData[ix] > maxGray) 
                maxGray = histogramData[ix];
        }
        double hunit = maxGray / imgGray.getHeight();
        int wstep = 0;
        for (int grayIx = 0; grayIx < histogramData.length; grayIx++) {
           int div = Math.round(histogramData[grayIx]);
           int barHeight = 0;
           if (div != 0)
               barHeight = (int) imgGray.getHeight() / Math.round(histogramData[grayIx]);
           g2d.drawLine(wstep, imgGray.getHeight(), wstep, imgGray.getHeight() - barHeight);
           g2d.drawLine(wstep, imgGray.getHeight() - barHeight, wstep + w, imgGray.getHeight() - barHeight);
           g2d.drawLine(wstep + w, imgGray.getHeight() - barHeight, wstep + w, imgGray.getHeight());
           // next width
           wstep += w;
        }
        g2d.dispose();
        showTextInList("Histogram");
        showImageInList(this.imgHistogram);
        writebw(new File(this.fileName + "_h.jpg"), this.imgHistogram, "jpg");
            
            
        /*
        this.imgBr = ImageUtils.modifyImageBrightness(this.imgMedian, 0.375f, 0.10f);
        showTextInList("After Brightness correction");
        showImageInList(this.imgBr);
        */
        
        this.imgHist = HistogramEqualization.execute(this.imgGray);
        showTextInList("After Histogram equalization");
        showImageInList(this.imgHist);
        
        this.imgOtsuBin = ImageUtils.convertToBinaryWithOtsu(this.imgHist, 0);
        if (this.isStoreTempFiles) {
//            writebw(new File(this.fileName + "_1_ob.png"), this.imgOtsuBin, "png");
            showTextInList("After Otru binarization");
            showImageInList(this.imgOtsuBin);
        }        
        
        this.binCommonColor = detectMostCommonColor(this.imgOtsuBin);
        if (this.binCommonColor.equals(Color.BLACK)) {
            this.imgInv = invertImage(imgOtsuBin);
            if (this.isStoreTempFiles) {
//              writebw(new File(this.fileName + "_1_i.png"), this.imgInv, "png");
              showTextInList("After Convert to inverse");
              showImageInList(this.imgInv);
          }        
        }
        
        
        // remove noise
        this.imgReduced = ImageUtils.resizeImage(this.imgOtsuBin, originWidth / ImageUtils.RESIZE_RATIO, originHeight / ImageUtils.RESIZE_RATIO, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        if (this.isStoreTempFiles) {
//            writebw(new File(this.fileName + "_2_r.png"), this.imgReduced, "png");
            showTextInList("After Reduce size / " + ImageUtils.RESIZE_RATIO);
            showImageInList(this.imgReduced);
        }

        
        /*
        this.imgGray = ImageUtils.convertToGrayWithFilter(this.imgReduced);
        if (this.isStoreTempFiles) {
//          writebw(new File(this.fileName + "_2_r.png"), this.imgReduced, "png");
          showTextInList("Gray");
          showImageInList(this.imgGray);
      }
        

        this.imgClosed = ImageUtils.closingImage(this.imgGray, StructElementShape.CIRCLE, 3);
        if (this.isStoreTempFiles) {
//          writebw(new File(this.fileName + "_2_r.png"), this.imgReduced, "png");
          showTextInList("Closed");
          showImageInList(this.imgClosed);
      }

        */

        
        this.imgORRb = ImageUtils.resizeImage(this.imgReduced, originWidth, originHeight, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        if (this.isStoreTempFiles) {
//            writebw(new File(this.fileName + "_3_rb.png"), this.imgORRb, "png");
            showTextInList("After Expand size * " + ImageUtils.RESIZE_RATIO);
            this.imgORRbMark = ImageUtils.drawBorderOnTheImage(this.imgORRb, Color.RED);
            showImageInList(this.imgORRbMark);
        }        

        
        Rectangle rectangle = calculateBoundingBoxWithProjection(this.imgORRb);        
        this.imgORRb = ImageUtils.drawBorderOnTheImage(this.imgORRb, rectangle, Color.GREEN);
        showTextInList("Marked Crop Rect");
        showImageInList(this.imgORRb);
        
        try {
            //rectangle = adjustRectangle(rectangle, originWidth, originHeight, 30);
            this.imgSub = cropSubimage(this.img, rectangle);
            this.imgORRbSub = cropSubimage(this.imgORRb, rectangle);
            if (this.isStoreTempFiles) {
//                writebw(new File(this.fileName + "_sub.png"), this.imgSub, "png");
                showTextInList("After Corop");
                showImageInList(this.imgSub);
            }            
        } catch (Exception e) {
            System.out.println("Can't crop! " + this.fileName + " x " + rectangle.x  + " y " + rectangle.y + " w " + rectangle.width + " h " + rectangle.height);
            System.err.println(e.getMessage());
            isSuccess = false;
        }        
        
        int RLS_Threshold = 10;        
        this.imgRLS = ImageUtils.executeRLSA(this.imgORRbSub, RLS_Threshold, RunLengthSmoothingKind.BOTH_AND);
        if (this.isStoreTempFiles) {
//            writebw(new File(this.fileName + "_2rls.png"), this.imgRLS, "png");
            showTextInList("After RLSA");
            showImageInList(this.imgRLS);
        }        
        
        return number;
    }
}
